const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const app = express();
const fileUpload = require('express-fileupload');

const db = require('./db');

const daysController = require('./controllers/days');
const feedsController = require('./controllers/feed');
const usersController = require('./controllers/users');
const adminsController = require('./controllers/admins');

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/feed', fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));

//db.connect('mongodb://mongo:27017/vocapp', (err) => {
  db.connect('mongodb://127.0.0.1:27017/vocapp', (err) => {
  if (err) return console.log(err);

  app.listen(3012, () => {
    console.log('API app started!');
  });
})

app.post('/admins', adminsController.all);
app.get('/poloalto', adminsController.allall);
// app.post('/poloalto/add', adminsController.createcreate);
// app.get('/admins/:id', adminsController.findById);
app.post('/admins/add', adminsController.create);
app.post('/admins/login', adminsController.login);
app.post('/admins/:id/delete', adminsController.delete);
app.put('/admins/:id', adminsController.update);

app.get('/users', usersController.all);
app.get('/:langF/:langT/:level/user/:userID', usersController.getDay);
app.get('/:langF/:langT/:level/user/:userID/notprogress', usersController.getDayNotProgrss);
app.get('/user/:userID/info', usersController.findById);
app.get('/user/:userID/history', usersController.history);
app.get('/:langF/:langT/feedlist/:userID', usersController.feedlist);
app.get('/feedlist/:userID/description/:postID', usersController.description);
app.get('/feedlist/:userID/like/:postID', usersController.like);
app.get('/feedlist/:userID/unlike/:postID', usersController.unlike);
// app.delete('/user/:userID', usersController.delete);

//v2
app.get('/v2/:langF/:langT/:level/user/:userID', usersController.getDayV2);

app.get('/langs', daysController.getAllLangs);
app.post('/dayslist', daysController.all);
app.post('/:langF/:langT/dayslist', daysController.allSorted);
app.post('/:langF/:langT/dayslist/:level', daysController.allSortedLevel);
app.get('/day/:id', daysController.findById);
app.post('/day', daysController.create);
app.put('/day/:id', daysController.update);
app.post('/day/:id/delete', daysController.delete);

app.post('/feedlist', feedsController.all);
app.get('/feed/:id', feedsController.findById);
app.post('/feed', feedsController.create);
app.put('/feed/:id', feedsController.update);
app.post('/feed/:id/delete', feedsController.delete);
app.post('/:langF/:langT/feedlist', feedsController.allSorted);

app.get('/', (req, res) => {
  res.send('What did u want to see here?');
});
