const AdminList = require('../models/admins');
const ObjectID = require('mongodb').ObjectID;

exports.all = (req, res) => {
  const login = req.body.login || {email:"null", password:"null"};

  AdminList.login(login, (err, user) => {
    if(user && user.permission > 1) {
      AdminList.all((err, docs) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        return res.send(docs);
      })
    } else {
      res.sendStatus(500);
    }
  });
}
exports.allall = (req, res) => {
  AdminList.all((err, docs) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(docs);
  })
}

exports.findById = (req, res) => {
  AdminList.findById(req.params.id, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send({name: doc.name, email: doc.email, langs: doc.langs, permission: doc.permission});
  })
}

exports.login = (req, res) => {
  
  const login = req.body.login || {email:"null", password:"null"};

  AdminList.login(login, (err, user) => {
    if(user && user.permission > -1) {
      AdminList.setToken(ObjectID(user._id), (err, token) => {
        const userData = {
          name: user.name,
          email: user.email,
          langs: user.langs,
          currentLangs: user.langs[0],
          permission: user.permission,
          token: token,
        };

        return res.send(userData);
      });
    } else {
      res.sendStatus(500);
    }
  });
}

exports.createcreate = (req, res) => {
  const admin = {
    name: req.body.name,
    password: req.body.password,
    email: req.body.email,
    langs: req.body.langs,
    permission: req.body.permission,
  };

  AdminList.create(admin, (err, usr) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    return res.sendStatus(200);
  });
}

exports.create = (req, res) => {
  const admin = {
    name: req.body.name,
    password: req.body.password,
    email: req.body.email,
    langs: req.body.langs,
    permission: 0,
  };

  const login = req.body.login || {email:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > 1) {
      AdminList.find({email: admin.email}, (err, usr) => {
        if(!usr) {
          AdminList.create(admin, (err, usr) => {
            if(err) {
              console.log(err);
              return res.sendStatus(500);
            }
      
            return res.sendStatus(200);
          });
        } else {
          return res.send('Email is exist!');
        }
      });
    } else {
      res.sendStatus(500);
    }
  });
}

exports.update = (req, res) => {
  const newData = req.body.user;

  const login = req.body.login || {email:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > 1) {
      AdminList.update(req.params.id, newData, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.sendStatus(200);
      })
    } else {
      res.sendStatus(500);
    }
  });
}

exports.delete = (req, res) => {
  const login = req.body.login || {email:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > 1) {
      AdminList.delete(req.params.id, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.sendStatus(200);
      });    
    } else {
      res.sendStatus(500);
    }
  });
}