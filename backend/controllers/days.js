const DaysList = require('../models/days');
const AdminList = require('../models/admins');

exports.all = (req, res) => {
  const login = req.body || { username: "null", password: "null" };

  AdminList.login(login, (err, doc) => {
    if (doc && doc.permission > -1) {
      DaysList.all((err, docs) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        res.send(docs);
      });
    } else {
      res.sendStatus(500);
    }
  });
}

exports.getAllLangs = (req, res) => {
  DaysList.getAllLangs((err, docs) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(docs);
  })
}

exports.allSorted = (req, res) => {
  const login = req.body || { username: "null", password: "null" };

  AdminList.login(login, (err, doc) => {
    if (doc && doc.permission > -1) {
      DaysList.allSorted((req.params.langF + req.params.langT), (err, docs) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        res.send(docs);
      }); 
    } else {
      res.sendStatus(500);
    }
  });
}

exports.allSortedLevel = (req, res) => {
  const login = req.body || { username: "null", password: "null" };

  AdminList.login(login, (err, doc) => {
    if (doc && doc.permission > -1) {
      DaysList.allSortedLevel((req.params.langF + req.params.langT), req.params.level, (err, docs) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        res.send(docs);
      });
    } else {
      res.sendStatus(500);
    }
  });
}

exports.findById = (req, res) => {
  DaysList.findById(req.params.id, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(doc);
  })
}

exports.create = (req, res) => {
  const day = {
    word: req.body.word,
    translate: req.body.translate,
    phrases: req.body.phrases,
    answers: req.body.answers,
    level: req.body.level,
    langs: req.body.langs,
  };

  const login = req.body.login || {username:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > -1) {
      DaysList.allSorted(day.langs, (err, docs) => {
        if(docs.length < 1) {
          const lang = {val: day.langs};
    
          DaysList.createLang(lang, (err, lang) => {
            if(err) {
              console.log(err);
              return res.sendStatus(500);
            }
          });
    
        }
      });
    
      DaysList.create(day, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        return res.send(day);
      });

    } else {
      res.sendStatus(500);
    }
  });
}

exports.update = (req, res) => {
  const newData = {
    word: req.body.word,
    translate: req.body.translate,
    phrases: [],
    answers: req.body.answers,
  };

  req.body.phrases.map((item, indx) => {
    if (item.text.length > 0 || item.translate.length > 0 || indx === 0) newData.phrases.push(item);
  })

  const login = req.body.login || {username:"null", password:"null"};

  AdminList.login(login, (err, usr) => {
    if(usr && usr.permission > -1) {
      DaysList.update(req.params.id, newData, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.send(doc);
      });

    } else {
      res.sendStatus(500);
    }
  });
}

exports.delete = (req, res) => {
  const login = req.body.login || {username:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > -1) {
      DaysList.delete(req.params.id, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.sendStatus(200);
      });

    } else {
      res.sendStatus(500);
    }
  });
}