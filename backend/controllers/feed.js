const FeedList = require('../models/feed');
const AdminList = require('../models/admins');

exports.allSorted = (req, res) => {
  const login = req.body || { username: "null", password: "null" };
  
  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > -1) {
      FeedList.allSorted((req.params.langF + req.params.langT), (err, docs) => {
      // FeedList.all((err, docs) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.send(docs);
      }); 
    } else {
      res.sendStatus(500);
    }
  });
}

exports.all = (req, res) => {
  const login = req.body || { username: "null", password: "null" };

  AdminList.login(login, (err, doc) => {
    if (doc && doc.permission > -1) {
      FeedList.all((err, docs) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        res.send(docs);
      });
    } else {
      res.sendStatus(500);
    }
  });
}

exports.findById = (req, res) => {
  FeedList.findById(req.params.id, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(doc);
  })
}

exports.create = (req, res) => {
  const feedData = {
    explanation: req.body.explanation,
    description: req.body.description,
    snapping: [],
    level: [],
    langs: req.body.langs,
    attached: '',
  };

  if (req.body.level) {
    const levelArr = req.body.level.split(',');
    levelArr.map((item) => {
      feedData.level.push(item);
    })
  }

  if (req.body.snapping) {
    const snappingArr = req.body.snapping.split(',');
    snappingArr.map((item) => {
      feedData.snapping.push(item);
    })
  }

  const email = req.body.email || "";
  const password = req.body.password || "";
  const token = req.body.token || "";
  const login = { email, password, token } || {username:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > -1) {
      FeedList.create(feedData, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        return res.send(feedData);
      });

    } else {
      res.sendStatus(500);
    }
  });
}

exports.update = (req, res) => {
  const newData = {
    explanation: req.body.explanation,
    description: req.body.description,
    snapping: [],
    level: [],
  };

  if (req.body.level) {
    const levelArr = req.body.level.split(',');
    levelArr.map((item) => {
      newData.level.push(item);
    })
  }

  if (req.body.snapping) {
    const snappingArr = req.body.snapping.split(',');
    snappingArr.map((item) => {
      newData.snapping.push(item);
    })
  }

  const email = req.body.email || "";
  const password = req.body.password || "";
  const token = req.body.token || "";
  const login = { email, password, token } || {username:"null", password:"null"};

  if (!req.files) {
    if(req.body.attached && req.body.attached==='null'){
      newData.attached = '';
    }

    AdminList.login(login, (err, usr) => {
      if(usr && usr.permission > -1) {
        FeedList.update(req.params.id, newData, (err, doc) => {
          if(err) {
            console.log(err);
            return res.sendStatus(500);
          }
      
          res.send(doc);
        });

      } else {
        res.sendStatus(500);
      }
    });
  } else {
    const attachedFile = req.files.attached;
    const rand = Math.floor(Math.random() * (99999999 - 999)) + 999;
  
    attachedFile.mv(`./feedFiles/${rand}${attachedFile.name}`, function (err) {
      if (err) {
        return res.status(500).send(err);
      }
  
      newData.attached = `feedFiles/${rand}${attachedFile.name}`;
  
      AdminList.login(login, (err, usr) => {
        if(usr && usr.permission > -1) {
          FeedList.update(req.params.id, newData, (err, doc) => {
            if(err) {
              console.log(err);
              return res.sendStatus(500);
            }
        
            res.send(doc);
          });
  
        } else {
          res.sendStatus(500);
        }
      });
    });
  }

}

exports.delete = (req, res) => {
  const login = req.body.login || {username:"null", password:"null"};

  AdminList.login(login, (err, doc) => {
    if(doc && doc.permission > -1) {
      FeedList.delete(req.params.id, (err, doc) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        res.sendStatus(200);
      });

    } else {
      res.sendStatus(500);
    }
  });
}