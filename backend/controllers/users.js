const Users = require('../models/users');
const DaysList = require('../models/days');
const FeedList = require('../models/feed');

exports.all = (req, res) => {
  Users.all((err, docs) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(docs);
  })
}

exports.findById = (req, res) => {
  Users.findById(req.params.userID, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }
    res.send(doc);
  })
}

exports.history = (req, res) => {
  Users.findById(req.params.userID, (err, user) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }
    
    let hist = {};
    Object.keys(user.progress).map((lang, indx, arr) => {
      DaysList.allSortedLevel(lang.slice(0, 6), (lang.slice(6, 7) || 0), (err, docs) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        hist[lang] = docs.slice(0, +user.progress[lang] + 1);
        if(indx === arr.length - 1) return res.send(hist);
      });
    });
  })
}

exports.info = (req, res) => {
  Users.info(userID, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }
    res.send(doc);
  })
}

exports.getDayV2 = (req, res) => {
  const langF = req.params.langF;
  const langT = req.params.langT;
  const level = req.params.level;
  const id = req.params.userID;

  Users.findById(id, (err, usr) => {
    if (err) {

      Users.create(id, langF, langT, level, (err, user) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        DaysList.allSortedLevel((langF + langT), level, (err, days) => {
          if (err) {
            console.log(err);
            return res.sendStatus(500);
          }

          return res.send([
            days[0],
            days[1],
            days[2],
            days[3],
          ]);
        });
      });

    } else {
      const newData = usr;
      const prgsVal = +newData.progress[langF + langT + level] > -1 ? (+newData.progress[langF + langT + level] + 1) : 0;
      newData.progress[langF + langT + level] = prgsVal;

      Users.update(id, newData, (err, user) => {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }

        DaysList.allSortedLevel((langF + langT), level, (err, days) => {
          if (err) {
            console.log(err);
            return res.sendStatus(500);
          }

          return res.send([
            days[prgsVal],
            days[prgsVal + 1],
            days[prgsVal + 2],
            days[prgsVal + 3],
          ]);
        });
      });

    }
  });
}

exports.getDay = (req, res) => {
  const langF = req.params.langF;
  const langT = req.params.langT;
  const level = req.params.level;
  const id = req.params.userID;

  Users.findById(id, (err, usr) => {
    if(err) {

      Users.create(id, langF, langT, level, (err, user) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        DaysList.allSortedLevel((langF + langT), level, (err, days) => {
          if(err) {
            console.log(err);
            return res.sendStatus(500);
          }

          return res.send(days[0]);
        });
      });

    } else {
      const newData = usr;
      const prgsVal = +newData.progress[langF + langT + level] > -1 ? (+newData.progress[langF + langT + level] + 1) : 0;
      newData.progress[langF + langT + level] = prgsVal;

      Users.update(id, newData, (err, user) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }
    
        DaysList.allSortedLevel((langF + langT), level, (err, days) => {
          if(err) {
            console.log(err);
            return res.sendStatus(500);
          }

          return res.send(days[prgsVal]);
        });
      });

    }
  });
}

exports.getDayNotProgrss = (req, res) => {
  const langF = req.params.langF;
  const langT = req.params.langT;
  const level = req.params.level;
  const id = req.params.userID;

  Users.findById(id, (err, usr) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    } else {
      const prgsVal = +usr.progress[langF + langT + level] > -1 ? +usr.progress[langF + langT + level] : 0;

      DaysList.allSortedLevel((langF + langT), level, (err, days) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }

        return res.send([
          days[prgsVal],
          days[prgsVal + 1],
          days[prgsVal + 2],
          days[prgsVal + 3],
        ]);
      });
    }
  });
}

exports.feedlist = (req, res) => {
  Users.findById(req.params.userID, (err, usr) => {
    if (err) {
      console.log(err);
      return res.sendStatus(500);
    }

    FeedList.allSorted((req.params.langF + req.params.langT), (err, docs) => {
      if (err) {
        console.log(err);
        return res.sendStatus(500);
      }

      const word = req.query.word;
      const newUserData = usr;
      let posts = [];
      
      if(!!!newUserData.postsHistory) newUserData.postsHistory = {};
      let likesCount = 0;
      docs.map((item) => ++likesCount);
      const average = Math.ceil(likesCount/docs.length);
      
      docs.map((item) => {
        if (item.snapping.indexOf(word) > -1) {
          if(!!!newUserData.postsHistory[item._id]) {
            posts.push(item);
          } else {
            const { lastOpened, views, like, description } = newUserData.postsHistory[item._id];
            if(!!lastOpened) {
              const lo = new Date(lastOpened);
              const now = new Date().getTime();

              if (!!!views || views < 3) {
                if (new Date(lo.setDate(lo.getDate() + 1)).getTime() > now) {
                  posts.push(item);
                } else {
                  if(lo.setDate(lo.getDate() + 7) < new Date().getTime()) {
                    if(lo.setDate(lo.getDate() + 41) < new Date().getTime()) {
                      posts.push(item);
                    } else {
                      if (lo.setDate(lo.getDate() + 21) > new Date().getTime()) {
                        posts.push(item);
                      } else {
                        if (!!!like && !!!description && lo.setDate(lo.getDate() + 21) < new Date().getTime()) {
                          posts.push(item);
                        } else {
                          if (item.likes && !!!like
                              && lo.setDate(lo.getDate() + 21) < new Date().getTime()
                              && item.likes > Math.ceil(average * 1.2)) {
                            posts.push(item);
                          }
                        }
                      }
                    }
                  }
                }
              }
            } else {
              posts.push(item);
            }
          }
        }
      });

      posts.map((item) => {
        if (!!!newUserData.postsHistory[item._id]) {
          newUserData.postsHistory[item._id] = {};
          newUserData.postsHistory[item._id].lastOpened = new Date().getTime();

          if (!!!newUserData.postsHistory[item._id].views) newUserData.postsHistory[item._id].views = 0;
          newUserData.postsHistory[item._id].views = +newUserData.postsHistory[item._id].views + 1;
        } else {
          if (newUserData.postsHistory[item._id].lastOpened) {
            const lastOpened = new Date(newUserData.postsHistory[item._id].lastOpened);
            const now = new Date();
  
            if (new Date(lastOpened.setDate(lastOpened.getDate() + 1)).getTime() < now.getTime()) {
              newUserData.postsHistory[item._id] = {};
              newUserData.postsHistory[item._id].lastOpened = new Date().getTime();
  
              if (!!!newUserData.postsHistory[item._id].views) newUserData.postsHistory[item._id].views = 0;
              newUserData.postsHistory[item._id].views = +newUserData.postsHistory[item._id].views + 1;
            }
          } else {
            newUserData.postsHistory[item._id].lastOpened = new Date().getTime();

            if (!!!newUserData.postsHistory[item._id].views) newUserData.postsHistory[item._id].views = 0;
            newUserData.postsHistory[item._id].views = +newUserData.postsHistory[item._id].views + 1;
          }
        }
      });

      Users.update(req.params.userID, newUserData, (err, doc) => {
        if(err) {
          return res.sendStatus(500);
        }
        
        res.send([posts, usr]);
      });
  
    }); 
  })
}

exports.description = (req, res) => {
  Users.findById(req.params.userID, (err, usr) => {
    if(err) {
      return res.sendStatus(500);
    }

    const newUserData = usr;
    if (!!!newUserData.postsHistory) newUserData.postsHistory = {};
    newUserData.postsHistory[req.params.postID].description = true;

    Users.update(req.params.userID, newUserData, (err, doc) => {
      if(err) {
        console.log(err);
        return res.sendStatus(500);
      }

      res.send(doc);
    })
  });
}

exports.like = (req, res) => {
  Users.findById(req.params.userID, (err, usr) => {
    if(err) {
      return res.sendStatus(500);
    }

    const newUserData = usr;
    if (!!newUserData.postsHistory) {
      if(!!newUserData.postsHistory[req.params.postID]) {
        newUserData.postsHistory[req.params.postID].like = true;
      } else {
        newUserData.postsHistory[req.params.postID] = {};
        newUserData.postsHistory[req.params.postID].like = true;
      }
    } else {
      newUserData.postsHistory = {};
      newUserData.postsHistory[req.params.postID] = {};
      newUserData.postsHistory[req.params.postID].like = true;
    }

    Users.update(req.params.userID, newUserData, (err, doc) => {
      if(err) {
        console.log(err);
        return res.sendStatus(500);
      }
      FeedList.findById(req.params.postID, (err, post) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }

        const newPostData = post;
        if(!!newPostData.likes) {
          newPostData.likes = +newPostData.likes + 1;
        } else {
          newPostData.likes = 1;
        }

        FeedList.update(req.params.postID, newPostData, (err, doc) => {
          if(err) {
            console.log(err);
            return res.sendStatus(500);
          }
      
          res.send([post, usr]);
        })
      })
    })
  });
}

exports.unlike = (req, res) => {
  Users.findById(req.params.userID, (err, usr) => {
    if(err) {
      return res.sendStatus(500);
    }

    const newUserData = usr;
    if (!!newUserData.postsHistory) {
      if(!!newUserData.postsHistory[req.params.postID]) {
        newUserData.postsHistory[req.params.postID].like = false;
      } else {
        newUserData.postsHistory[req.params.postID] = {};
        newUserData.postsHistory[req.params.postID].like = false;
      }
    } else {
      newUserData.postsHistory = {};
      newUserData.postsHistory[req.params.postID] = {};
      newUserData.postsHistory[req.params.postID].like = false;
    }

    Users.update(req.params.userID, newUserData, (err, doc) => {
      if(err) {
        console.log(err);
        return res.sendStatus(500);
      }
      FeedList.findById(req.params.postID, (err, post) => {
        if(err) {
          console.log(err);
          return res.sendStatus(500);
        }

        const newPostData = post;
        if(!!newPostData.likes) {
          newPostData.likes = +newPostData.likes - 1;
        } else {
          newPostData.likes = 0;
        }

        FeedList.update(req.params.postID, newPostData, (err, doc) => {
          if(err) {
            console.log(err);
            return res.sendStatus(500);
          }
      
          res.send([post, usr]);
        })
      })
    })
  });
}

exports.create = (req, res) => {
  Users.create(req.params.userID, req.params.langF, req.params.langT, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(user);
  })
}

exports.update = (req, res) => {
  const newData = {
    word: req.body.word,
    translate: req.body.translate,
  };

  Users.update(req.params.userID, newData, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.sendStatus(200);
  })
}

exports.delete = (req, res) => {
  Users.delete(req.params.userID, (err, doc) => {
    if(err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.sendStatus(200);
  });
}