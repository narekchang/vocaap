const db = require('../db');
const ObjectID = require('mongodb').ObjectID;
const sha1 = require("sha1");

exports.all = (cb) => {
  db.get().collection('admins').find().toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.findById = (id, cb) => {
  db.get().collection('admins').findOne({ _id: ObjectID(id) }, (err, doc) => {
    cb(err, doc);
  });
}

exports.find = (keys, cb) => {
  db.get().collection('admins').findOne(keys, (err, doc) => {
    cb(err, doc);
  });
}

exports.login = (loginData, cb) => {
  const login = loginData.token
    ? { token: loginData.token }
    : { email: loginData.email, password: loginData.password };

  db.get().collection('admins').findOne(login, (err, doc) => {
    cb(err, doc);
  });
}

exports.setToken = (id, cb) => {
  const newToken = sha1(id);

  db.get().collection('admins').updateOne({ _id: id }, {$set: {token: newToken}}, (err, result) => cb(err, newToken));
}

exports.create = (usr, cb) => {
  db.get().collection('admins').insert(usr, (err, result) => {
    cb(err, result);
  });
}

exports.update = (id, newData, cb) => {
  db.get().collection('admins').updateOne({ _id: ObjectID(id) }, newData, (err, result) => cb(err, result));
}

exports.delete = (id, cb) => {
  db.get().collection('admins').deleteOne({ _id: ObjectID(id) }, (err, result) => cb(err, result));
}