const db = require('../db');
const ObjectID = require('mongodb').ObjectID;

exports.all = (cb) => {
  db.get().collection('days').find().toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.getAllLangs = (cb) => {
  db.get().collection('langs').find().toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.allSorted = (langs, cb) => {
  db.get().collection('days').find({ langs: langs }).toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.allSortedLevel = (langs, level, cb) => {
  db.get().collection('days').find({ langs, level: +level }).toArray((err, docs) => {
    cb(err, docs);
  });
}


exports.findById = (id, cb) => {
  db.get().collection('days').findOne({ _id: ObjectID(id) }, (err, doc) => {
    cb(err, doc);
  });
}

exports.create = (day, cb) => {
  db.get().collection('days').insert(day, (err, result) => {
    cb(err, result);
  });
}

exports.createLang = (lang, cb) => {
  db.get().collection('langs').insert(lang, (err, result) => {
    cb(err, result);
  });
}

exports.update = (id, newData, cb) => {
  db.get().collection('days').updateOne({ _id: ObjectID(id) }, {$set: newData}, (err, result) => cb(err, result));
}

exports.delete = (id, cb) => {
  db.get().collection('days').deleteOne({ _id: ObjectID(id) }, (err, result) => cb(err, result));
}