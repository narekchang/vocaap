const db = require('../db');
const ObjectID = require('mongodb').ObjectID;

exports.all = (cb) => {
  db.get().collection('feed').find().toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.allSorted = (langs, cb) => {
  db.get().collection('feed').find({ langs: langs }).toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.findById = (id, cb) => {
  db.get().collection('feed').findOne({ _id: ObjectID(id) }, (err, doc) => {
    cb(err, doc);
  });
}

exports.create = (post, cb) => {
  db.get().collection('feed').insert(post, (err, result) => {
    cb(err, result);
  });
}

exports.update = (id, newData, cb) => {
  db.get().collection('feed').updateOne({ _id: ObjectID(id) }, { $set: newData }, (err, result) => cb(err, result));
}

exports.delete = (id, cb) => {
  db.get().collection('feed').deleteOne({ _id: ObjectID(id) }, (err, result) => cb(err, result));
}
