const db = require('../db');
const ObjectID = require('mongodb').ObjectID;

exports.all = (cb) => {
  db.get().collection('users').find({}).toArray((err, docs) => {
    cb(err, docs);
  });
}

exports.findById = (userID, cb) => {
  db.get().collection('users').findOne({ userID: userID }, (err, doc) => {
    if(!!!doc) cb(true, doc);
    else cb(err, doc);
  });
}

exports.info = (userID, cb) => {
  db.get().collection('users').findOne({ userID: userID }, (err, doc) => {
    cb(err, doc);
  });
}

exports.getDay = (userID, langF, langT, cb) => {
  db.get().collection('users').findOne({ userID: userID }, (err, doc) => {
    cb(err, doc);
  });
}

exports.create = (userID, langF, langT, level, cb) => {
  const progressVal = {};
  progressVal[langF + langT + level] = 0;

  const user = {
    userID: userID,
    progress: progressVal,
  };

  db.get().collection('users').insert(user, (err, result) => {
    cb(err, result);
  });
}

exports.update = (userID, newData, cb) => {
  db.get().collection('users').updateOne({ userID: userID }, newData, (err, result) => cb(err, result));
}

exports.delete = (id, cb) => {
  db.get().collection('users').deleteOne({ _id: ObjectID(id) }, (err, result) => cb(err, result));
}