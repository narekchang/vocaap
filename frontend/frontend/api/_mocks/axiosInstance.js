import brand from './brand';

export default {
  get: (path) => {

    if (/^brands/.test(path)) {
      return new Promise(resolve => resolve({
        data: {
          status: 200,
          data: {
            brands: Array.from('123').map(id => ({ ...brand, id })),
            totalCount: 3,
          },
        },
      }));
    }


    return new Promise(resolve => resolve({
      data: {
        status: 200,
        data: {
          brands: [],
          totalCount: 3,
        },
      },
    }));
  },
};