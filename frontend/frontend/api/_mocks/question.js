export default {
  id: '1289653',
  text: 'Какой это символ?',
  number: 2,
  imageURL: 'https://s3-eu-central-1.amazonaws.com/wff-test/088ae8cc-8a84-4822-9bb0-50df919d1c63',
  answers: [
    { id:'3', text:'Один' },
    { id:'4', text:'Два' },
    { id:'5', text:'Три' },
    { id:'6', text:'Четыре' },
  ],
};