export default {
  id: '3',
  name: 'Quiz2',
  shortDesc: 'Краткое',
  desc: 'Полное',
  imageNarrowURL: 'https://s3-eu-central-1.amazonaws.com/wff-test/aa5c4258-dc6c-4d83-881a-e814c09b43c2',
  imageWideURL: 'https://s3-eu-central-1.amazonaws.com/wff-test/bd33445f-6a25-4332-9464-1d1884e79bce',
  endTime: 1497179721206,
  complexity: 'medium',
  status: 'active',
  textColor: '#dbf8f9',
  backgroundColor: '#2009b7',
  passedTasksCount: 1,
  totalTasksCount: 3,
  prizes: [],
};
