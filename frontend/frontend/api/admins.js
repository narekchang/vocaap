import axios from 'axios';

import { url, token } from './config';

export const getAdminsList = async () => {
  const res = await axios.post(`${url}/admins`, { login:{ token } });
  const { data } = res;

  return data;
};

export const createAdmin = async (adminData) => {
  const res = await axios.post(`${url}/admins/add`, { ...adminData, login:{ token } });
  const { data } = res;

  return data;
};

export const deleteAdmin = async (id) => {
  const res = await axios.post(`${url}/admins/${id}/delete`, { login:{ token } });
  const { data } = res;

  return data;
};