import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { url } from './../api/config';

if (process && process.env && process.env.NODE_ENV && process.env.NODE_ENV === 'test') {
  axios.defaults.adapter = httpAdapter;
}

const axiosInstance = axios.create({
  baseURL: url,
  /* other custom settings */
});

export const notAuthorizedReason = {
  status: 401,
  response: {
    data: {
      status: 'error',
      data: {
        text: "не авторизован",
        description: "обработано js",
      },
    },
  },
};

export const serverErrorInterceptor = axiosInstance.interceptors.response.use(response => response, error => {
  if (error.status === 500) return Promise.reject({
    status: 500,
    response: {
      data: {
        status: 'error',
        data: {
          text: "ошибка сервера (interceptor)",
          description: "обработано js",
        },
      },
    },
  });
  return Promise.reject(error);
});


export default axiosInstance;
