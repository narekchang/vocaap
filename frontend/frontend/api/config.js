// const scheme = 'https://';
// const domain = 'vocapp.ru';
// const port = ':81';

const scheme = 'http://';
const domain = 'localhost';
const port = ':3012';

const basePath = "/v1";


export const dir = `${scheme}${domain}${port}`;
export const url = `${scheme}${domain}${port}`;
// export const url = '/v1';

const localUserData = JSON.parse(localStorage.getItem("reduxPersist:user"));
export const token = localUserData ? localUserData.token : '';

export default {
  scheme,
  domain,
  port,
  basePath,
  url,
  token,
};