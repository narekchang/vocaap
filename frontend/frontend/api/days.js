import axios from 'axios';

import { url, token } from './config';

export const getDays = async (langs, level) => {
  const ll = langs.length / 2;
  const langF = langs.slice(0, ll);
  const langT = langs.slice(ll, (ll * 2));

  const res = await axios.get(`${url}/${langF}/${langT}/dayslist/${level}`);
  const days = res.data;

  return days;
};

export const createDay = async (dayData) => {
  const res = await axios.post(`${url}/day`, { ...dayData, login:{ token } });
  const day = res.data;

  return day;
};

export const updateDayData = async (id, dayData) => {
  const res = await axios.put(`${url}/day/${id}`, { ...dayData, login:{ token } });
  const day = res.data;

  return day;
};

export const deleteDay = async (id) => {
  const res = await axios.post(`${url}/day/${id}/delete`, { id, login:{ token } });
  const response = res.data;

  return response;
};