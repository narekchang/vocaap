import axios from 'axios';

import { url, token } from './config';

export const getPosts = async (langs, level) => {
  const res = await axios.post(`${url}/feedlist`);
  const posts = res.data;

  return posts;
};
export const createPost = async (postData) => {
  const res = await axios.post(`${url}/post`, { ...postData, login:{ token } });
  const post = res.data;

  return post;
};

export const updatePostData = async (id, postData) => {
  const res = await axios.put(`${url}/post/${id}`, { ...postData, login:{ token } });
  const post = res.data;

  return post;
};

export const deletePost = async (id) => {
  const res = await axios.post(`${url}/post/${id}/delete`, { id, login:{ token } });
  const response = res.data;

  return response;
};