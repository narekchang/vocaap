import axios from 'axios';

import { url } from './config';

export const userAuth = async (login) => {
  const res = await axios.post(`${url}/admins/login`, { login:{ ...login } });
  const { data } = res;

  return data;
};

export const getAllLangs: () => Promise = async () => {
  const res = await axios.get(`${url}/langs`, {});
  const { data } = res;

  return data;
};

export const userProfile = () => [];
