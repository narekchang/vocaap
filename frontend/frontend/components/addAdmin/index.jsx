//@flow
import React from 'react';

import { createAdmin } from '_api/admins';
import { getAllLangs } from '_api/user';

import s from './style.styl';
import ms from '../../style.styl';

export default class AddAdmin extends React.Component<{self:Object}> {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      langs: this.props.self.state.adminLang || '',
      password: '',
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]:e.target.value });
  }

  add = () => {
    const adminData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
    };

    adminData.langs = this.state.langs.length > 0 ? this.state.langs.replace('/', '').toLowerCase().split() : [''];
    
    if (adminData.name.length > 0
        && adminData.email.length > 0
        && adminData.langs.length > 0
        && adminData.password.length > 0)
    {
      createAdmin(adminData).then(() => this.props.self.setState({ drop:true, addShow:false, moderatorsList:true }));
    }
  }

  render() {

    return (
      <div className={`${s.wrapper}`}>
        <div className={s.fieldBlock}>
          <input
            type="text"
            placeholder="Пара языков"
            className={ms.field}
            name='langs'
            value={this.state.langs}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div className={s.fieldBlock}>
          <input
            type="text"
            placeholder="Имя переводчика"
            className={ms.field}
            name='name'
            value={this.state.name}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div className={s.fieldBlock}>
          <input
            type="text"
            placeholder="Почта для переводчика"
            className={ms.field}
            name='email'
            value={this.state.email}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div className={s.fieldBlock}>
          <input
            type="text"
            placeholder="Пароль для переводчика"
            className={ms.field}
            name='password'
            value={this.state.password}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div className={s.btnWrap}>
          <button onClick={() => this.add()} className={`${ms.btn} ${ms.btnBlue} ${ms.ttN}`}>Добавить</button>
        </div>
      </div>
    );
  }
}