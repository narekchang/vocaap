import dateFns from 'date-fns';
import ruLocale from 'date-fns/locale/ru';

export const format = (date, formatStr) => dateFns.format(date, formatStr, { locale:ruLocale });
export const distanceInWordsToNow = (date) => dateFns.distanceInWordsToNow(date, { locale:ruLocale });
export const distanceInWordsStrict = (dateToCompare, date, options) => dateFns.distanceInWordsStrict(dateToCompare, date, { ...options, locale:ruLocale });
export default dateFns;
