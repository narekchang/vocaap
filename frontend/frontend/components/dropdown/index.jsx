import React from 'react';
import onClickOutside from "react-onclickoutside";
import { withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setLangs } from '_pages/login/redux/actions';
import { loadDays } from '_pages/admin-panel/redux/actions';

import AddAdmin from '_components/addAdmin';
import ModeratorsInfo from '_components/moderatorsinfo';

import s from './style.styl';

class DropDown extends React.Component<{ List:Object, userData:Object, setLangs:Object, loadDays:Object }> {
  constructor(props) {
    super(props);

    this.state = {
      drop: false,
      addShow: false,
      adminLang: '',
      moderatorsList: false,
    };
  }

  handleClickOutside = () => {
    this.setState({
      drop: false,
      addShow: false,
      moderatorsList: false,
    });
  };

  toggleDrop(){
    this.setState({
      drop: !this.state.drop,
    });
  }

  changeLang(id) {
    const { List } = this.props;

    this.props.setLangs(List[id]);
    this.props.loadDays(List[id], this.props.userData.level);

    this.toggleDrop();
  }

  addNewAdmin() {
    this.setState({ addShow:true, moderatorsList:false });
  }

  changeAdminLang(newVal) {
    this.setState({ adminLang:newVal });
    console.log(this.state.adminLang);
  }

  hideModeratorsList() {
    this.setState({ moderatorsList:false, addShow:false });
  }

  showModeratorsList() {
    this.setState({ moderatorsList:true, addShow:false });
  }

  render() {
    const { List, userData } = this.props;
    const { addShow, drop, moderatorsList } = this.state;

    return (
      <div className={`${s.dropdown}`}>
        <button onClick={() => this.toggleDrop()} className={s.current}>
          {userData.currentLangs.slice(0, 3)}/{userData.currentLangs.slice(3, 6)}
        </button>
        {
          (List.length > 0 && drop) && (
            <div className={s.list}>
              {
                List.map((item, indx) => (
                  <div className={s.itemWrap} key={Math.floor(Math.random() * 9999)}>
                    <NavLink to="/panel" onClick={() => this.changeLang(indx)} key={`${indx * item.length}`} className={s.item}>
                      words {item.slice(0, 3)}/{item.slice(3, 6)}
                    </NavLink>
                    <NavLink to='/feed' className={s.item}>
                      feed {item.slice(0, 3)}/{item.slice(3, 6)}
                    </NavLink>
                    {userData.permission > 1 && (<ModeratorsInfo self={this} lang={item} />)}
                  </div>
                ))
              }
              {
                userData.permission > 1 && (
                  <div className={s.itemWrap}>
                    <button onClick={() => this.addNewAdmin()} className={`${s.item} ${s.itemAddNew}`}>
                      Add new
                    </button>
                  </div>
                )
              }
            </div>
          )
        }
        {(drop && addShow) && (<AddAdmin self={this} />)}
      </div>
    )
  }
}

export default withRouter(connect(
  state => ({ userData:state.user }),
  dispatch => bindActionCreators({ setLangs, loadDays }, dispatch),
)(onClickOutside(DropDown)));