import React from 'react';
import onClickOutside from "react-onclickoutside";
import Textarea from "react-textarea-autosize";

import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';

import mStyles from '../../style.styl';
import styles from './style.styl';

class InputPicker extends React.Component<{position:string, value: any}> {
  constructor(props) {
    super(props);
    this.state = {
      textField: this.props.value,
      emojiWindow: false,
    };
  }

  handleClickOutside = () => {
    this.setState({ emojiWindow:false });
  };

  selectEmoji(emoji) {
    const newVal = this.state.textField + emoji.native;
    this.setState({
      textField: newVal,
    });
  }

  callEmojiWindow() {
    this.setState({ emojiWindow:true });
  }
  
  handleChange(event) {
    this.setState({ textField:event.target.value });
  }

  render() {
    const { emojiWindow, textField } = this.state;
    const { position } = this.props;

    return (
      <div className={`${styles.inputEmojiPicker} ${position}`}>
        <Textarea
          type="text"
          placeholder="In English with emoji"
          className={`${mStyles.field} ${mStyles.col12} ${styles.inputEmojiPickerForm}`}
          value={textField}
          onChange={this.handleChange.bind(this)}
        />
        <svg className={styles.emojiIc} onClick={this.callEmojiWindow.bind(this)} width="18" height="18" viewBox="0 0 18 18">
          <g fill="none">
            <path fill="#686868" d="M8.992 1.5C4.853 1.5 1.5 4.86 1.5 9c0 4.14 3.353 7.5 7.492 7.5 4.148 0 7.508-3.36 7.508-7.5 0-4.14-3.36-7.5-7.508-7.5zM9 15c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm2.625-6.75c.623 0 1.125-.503 1.125-1.125S12.248 6 11.625 6 10.5 6.503 10.5 7.125s.502 1.125 1.125 1.125zm-5.25 0c.622 0 1.125-.503 1.125-1.125S6.997 6 6.375 6 5.25 6.503 5.25 7.125 5.753 8.25 6.375 8.25zM9 13.125a4.122 4.122 0 0 0 3.832-2.625H5.167A4.122 4.122 0 0 0 9 13.125z" />
          </g>
        </svg>
        
        {emojiWindow && (
          <Picker
            set='apple'
            emojisToShowFilter={false}
            showPreview={false}
            showSkinTones={false}
            sheetSize={32}
            onSelect={(emoji) => this.selectEmoji(emoji)}
          />)
        }
      </div>
    );
  }
}

export default onClickOutside(InputPicker);