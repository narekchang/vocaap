//@flow
import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getAdminsList, deleteAdmin } from '_api/admins'

import s from './style.styl';
import ms from '../../style.styl';

class ModeratorsInfo extends React.Component<{lang:String, self:Object}> {
  constructor(props) {
    super(props);

    this.state = {
      admList: [],
    };
  }

  componentWillMount() {
    this.updateList();  
  }

  updateList() {
    getAdminsList().then((res) => this.setState({ admList:res }));
  }

  removeAdmin = (id) => {
    const confirm = window.confirm('Точно хотите удалить?');
    if (confirm) deleteAdmin(id).then(() => this.updateList());
  }

  addAdminInLang = () => {
    this.props.self.setState({ addShow:true, adminLang:this.props.lang, moderatorsList:false });
  }

  show() {
    this.props.self.changeAdminLang(this.props.lang);
    this.props.self.showModeratorsList();
  }

  render() {
    const { admList } = this.state;
    const { moderatorsList, adminLang } = this.props.self.state;
    const { lang } = this.props;

    return (
      <div className={`${s.wrapper}`}>
        <button className={s.ic} onClick={() => this.show()} />
        {
          (moderatorsList && adminLang === lang) && (
            <div className={s.list}>
              <div className={s.title}>INFO</div>
              {
                admList.map(item => {
                  if (item.permission < 2 && (item.langs.indexOf(lang) > -1 || item.langs.indexOf('all') > -1)) {
                    return (
                      <div className={s.item} key={Math.floor(Math.random() * 8888)}>
                        <button className={s.remove} onClick={() => this.removeAdmin(item._id)} />
                        <div className={s.punkt}>{item.name || item.username}</div>
                        <div className={s.punkt}>{item.email}</div>
                        <div className={s.punkt}>{item.password}</div>
                      </div>
                    );
                  }
                })
              }
              <div className={s.btnWrap}>
                <button onClick={() => this.addAdminInLang()} className={`${ms.btn} ${ms.btnBorder} ${ms.ttN}`}>Добавить еще</button>
              </div>
            </div>
          )}
      </div>
    );
  }
}

export default withRouter(connect(
  state => ({ userData:state.user }),
  null,
  // dispatch => bindActionCreators({ getAdmins }, dispatch),
)(ModeratorsInfo));