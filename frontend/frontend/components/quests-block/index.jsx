// @flow
import React from 'react';
import { NavLink } from 'react-router-dom';

import { getAllQuests } from '../../api/quests';
import QuestItem from './item';

import mainStyles from '../../style.styl';
import styles from './style.styl';

type QuestsBlockProps = {|
  limitTo: number,
  limitFrom: number,
  myQuests: [],
  className: ?string,
|};
type QuestsBlockState = {|
  allQuests: Array<Object>,
  quests: Array<Object>,
  questsReady: boolean,
|};

export default class QuestsBlock extends React.Component<QuestsBlockProps, QuestsBlockState> {
  static defaultProps = {
    limitTo: 9,
    limitFrom: 0,
  }

  constructor() {
    super();
    this.state = {
      allQuests: [],
      quests: [],
      questsReady: false,
    };
  }

  componentWillMount() {
    const { myQuests, limitFrom, limitTo } = this.props;
    
    this.uploadQuests(limitTo, limitFrom, myQuests);
  }

  uploadQuests(limitTo: number, limitFrom: number, myQuests: any){
    if (myQuests) {
      this.setState({
        allQuests: myQuests,
        quests: myQuests.slice(limitFrom, limitTo),
        questsReady: true,
      });
    } else {
      getAllQuests().then((questsList) => {
        const questsListSort = questsList.slice(limitFrom, limitTo);
        this.setState({
          allQuests: questsList,
          quests: questsListSort,
          questsReady: true,
        });
      }).catch(() => {
        this.setState({
          questsReady: true,
        });
      });
    }
  }

  loadMoreQuests(){
    const newLimitTo = this.state.quests.length + 3;
    this.uploadQuests(newLimitTo, 0, this.props.myQuests);
  }

  render() {
    const { quests, allQuests, questsReady } = this.state;
    const { className } = this.props;

    return (
      <div className={mainStyles.row}>
        {allQuests.length < 1 && (
          <div className={styles.haventQuests}>
            <div className={styles.desc}>Пока что вы не прошли ни одного квеста. Нажмите «Смотреть квесты», чтобы посмотреть все доступные квесты</div>
            <NavLink to="/quests" className={`${mainStyles.btn} ${mainStyles.btnBlue} ${mainStyles.btnMiddle}`}>смотреть квесты</NavLink>
          </div>)
        }
        {!questsReady && (
          <div className={`${styles.questsWrap} ${styles.questsWrapEmpty}`}>
            <div className={styles.questsWrapEmptyRow}>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
              <div className={styles.quest}>
                <div className={styles.cover} />
                <div>
                  <div className={styles.questTitle}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDate}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                  <div className={styles.questDesc}>
                    <div className={styles.stroke} />
                    <div className={styles.stroke} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {questsReady && (
          <div>
            <div className={`${styles.questsWrap} ${className}`}>
              {quests.length < 1 && (
                <div>К сожаление, сейчас квестов нет.</div>
              )}
              {quests.map(item => <QuestItem key={item.id} id={item.id} title={item.title} story={item.story || item.description} timings={item.timings || ''} prizes={item.prizes || ''} image={item.background_url || ''} />)}
            </div>
            <div className={`${mainStyles.row} ${mainStyles.taCenter}`}>
              {quests.length < allQuests.length && (
                <button to="/quests" className={[ mainStyles.btn, mainStyles.btnBlue, styles.btnHome ].join(' ')} onClick={() => { this.loadMoreQuests(); }}> Еще квесты </button>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}