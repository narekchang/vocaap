import React from 'react';

import { NavLink } from 'react-router-dom';
import 'moment-timezone';
import 'moment/locale/ru';
import Moment from 'react-moment';

import _ from 'lodash';
// import { mapProps } from 'recompose';

import StatusQuestTag from '../quest-status-tag';

import styles from './style.styl';

const QuestItem = ({
  title,
  story,
  image,
  id,
  timings,
  prizes,
}: {
    title: string,
    story: string,
    image: string,
    id: string,
    timings: {},
    prizes: [],
}) => (
  <NavLink className={styles.quest} to={`/quest/${id}`} key={`quest-in-block_${id}`}>
    <div className={styles.cover}>
      <img src={image} alt="" />
      <StatusQuestTag quest={{ timings }} moreClass={styles.status} />
    </div>
    <div>
      <div className={styles.questTitle}>{title.substring(0, 55)}</div>
      {timings.end_date && (
        <div className={styles.questDate}>
          <span>Квест закончится </span>
          <Moment unix format="DD MMMM YYYY">{timings.end_date}</Moment>
        </div>
      )}

      {prizes.length < 1 ? (
        <div className={styles.questDesc} key={`quest-in-block_${id}__desc`}>{story.substring(0, 180)}...</div>
      ) : (
        <div className={styles.questPrizesBlock} key={`quest-in-block_${id}__desc`}>
          <div className={styles.questPrizesBlockTitle}>Призы Квесты:</div>
          <div>
            <div className={styles.questPrizesBlockItems}>
              <img src={prizes[0].image_url} alt="" className={styles.questPrizesBlockItem} />
              <img src={_.get(prizes[1], 'image_url') || ''} alt="" className={styles.questPrizesBlockItem} />
              <img src={_.get(prizes[2], 'image_url') || ''} alt="" className={styles.questPrizesBlockItem} />
            </div>
            <div className={styles.questPrizesBlockContent}>
              {prizes[0].title.substring(0, 20)}<span>и ещё 124 приза</span>
            </div>
          </div>
        </div>
      )}

    </div>
  </NavLink>
);

export default (QuestItem);