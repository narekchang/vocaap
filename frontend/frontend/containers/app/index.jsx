import React from 'react';
import { Route } from 'react-router';

import loadProgressBar from  '_components/progress-bar/index'

import TopBar from '_containers/topbar/index';

import LoginPage from '_pages/login';
import CheckPage from '_pages/check-page';
import AdminPanel from '_pages/admin-panel';
import AdminFeed from '_pages/admin-feed';
import Privacy from '_pages/privacy';
import Terms from '_pages/terms';

import styles from './style.styl';

loadProgressBar();

const App = () => (
  <div className={styles.app}>
    <div className={styles.container}>
      <Route exact path="*" component={TopBar} />
      <Route exact path="*" component={CheckPage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/panel" component={AdminPanel} />
      <Route path="/privacy-policy" component={Privacy} />
      <Route path="/terms-of-use" component={Terms} />
      <Route path="/feed" component={AdminFeed} />
    </div>
  </div>
);

export default App;