import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import DevTools from '../DevTools';

import App from '../app';
import { configureStore, history } from '../../state/configureStore';

let rootStore = null;

class Bootloader extends Component {
  state = {
    store: null,
  }

  async componentWillMount () {
    const store = await configureStore();
    rootStore = store;
    this.setState({ store });
  }

  render () {
    if (this.state.store === null) {
      return (
        <div>
          Booting...
        </div>
      );
    }

    return (
      <Provider store={this.state.store}>
        <ConnectedRouter history={history}>
          <div>
            <App />
            {process.env.NODE_ENV === 'development' && <DevTools />}
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export const getStore = () => rootStore;

export default Bootloader;