//@flow
import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userLogout, setLangs, getLangs } from '_pages/login/redux/actions';

import DropDown from '_components/dropdown';

import mainStyles from '../../style.styl';
import styles from './style.styl';

class TopBar extends React.Component
  <{
    userLogout: Object,
    history: Object,
    userData: Object,
    getLangs: Object,
    setLangs: Object
  }>{
  componentWillMount() {
    const { userData, setLangs, getLangs } = this.props;
    if (userData.auth){
      if (userData.langs[0] === 'all' || userData.currentLangs === 'all'){
        getLangs().then((langs) => setLangs(langs[0]));
      } else {
        setLangs(userData.currentLangs || userData.langs[0]);
      }
    }
  }

  logout() {
    this.props.userLogout().then(() => this.props.history.push('/login'));
  }

  render() {
    const { userData } = this.props;

    return (
      <div className={styles.sidebar} >
        <div className={mainStyles.content}>
          <div className={mainStyles.row}>
            <div className={styles.mainLogoWrap}>
              { (userData.auth)
                ? (<DropDown List={userData.langs} currentLangs={userData.currentLangs} />)
                : (<NavLink href="/" to="/" className={`${styles.mainLogo} ${mainStyles.link}`}>vocapp</NavLink>)
              }
            </div>
            {(userData.auth)
              ? (<NavLink href="/" to="/" className={styles.mainImage} />)
              : (<div className={styles.mainImage} />)
            }
            
            <div className={styles.rightBlockWrap}>
              { (userData.auth)
                ? (<button onClick={() => {this.logout()}} className={`${styles.rightBlock} ${mainStyles.link} ${mainStyles.logout}`}>{userData.name}</button>)
                : (<NavLink to='/login' className={mainStyles.link}>Log in</NavLink>)
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(
  state => ({ userData:state.user }),
  dispatch => bindActionCreators({ userLogout, setLangs, getLangs }, dispatch),
)(TopBar));