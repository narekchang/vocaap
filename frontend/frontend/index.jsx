import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import 'reset.css';

import Bootloader from './containers/bootloader';

import './style.styl';
import './fonts/include.jsfont';

render(
  <AppContainer>
    <Bootloader />
  </AppContainer>,
  document.querySelector("#app"),
);

if (module && module.hot) {
  module.hot.accept('./containers/bootloader', () => {
    // eslint-disable-next-line
    const Bootloader = require('./containers/bootloader').default;
    render(
      <AppContainer >
        <Bootloader />
      </AppContainer>,
      document.querySelector("#app"),
    );
  });
}
