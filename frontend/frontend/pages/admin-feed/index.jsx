// @flow
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactPaginate from 'react-paginate';

import PostItem from './item';

import { loadPosts, addPost, clearPost, updatePosts } from './redux/actions';
import { setLevel, setLangs, getLangs } from '../login/redux/actions';

import mStyles from '../../style.styl';
import styles from './style.styl';

class AdminFeed extends React.Component<{
  history: Object,
  userData: Object,
  posts: Object,
  editedPosts: Object,
  addPost: Object,
  updatePosts: Object,
  loadPosts: Object,
  setLevel: Object,
  setLangs: Object,
  getLangs: Object,
  clearPost: Object,
}> {
  constructor(props) {
    super(props);

    this.state = {
      activeItem: 0,
      searchQuery: '',
      currentPage: 1,
      showDropList: false,
      matchPostsKeys: [],
    };
  }

  componentWillMount() {
    this.checkAuth();
    this.loadPosts(this.props.userData.currentLangs, (this.props.userData.level || 0));

    const { userData, setLangs, getLangs } = this.props;
    if (userData.auth) {
      if (userData.langs[0] === 'all' || userData.currentLangs === 'all') {
        getLangs().then((langs) => setLangs(langs[0]));
      } else {
        setLangs(userData.currentLangs || userData.langs[0]);
      }
    }
  }

  setActiveItem(item) {
    this.setState({
      activeItem: item,
    });
  }

  setSearchQuery(e, notKeyPress) {
    const { matchPostsKeys } = this.state;
    
    if (((notKeyPress || e.keyCode === 13) && matchPostsKeys.length > 0) || e.target.value.length === 0) {
      this.setState({ currentPage:0 });
      this.setState({ searchQuery:e.target.value });
      this.setState({ showDropList:false });
    }
  }
  
  getPageCount = (arr) => {
    const arrLength = Object.keys(arr).length;
    let pageCount = Math.trunc(arrLength / 5);
    
    pageCount = arrLength % 5 > 0 ? pageCount + 1 : pageCount;
    
    return pageCount;
  }
  
  openDropList(e) {
    const { posts } = this.props;
    const matchPostsKeysCopy = [];
    const re = e.target.value.toLocaleLowerCase();

    Object.keys(posts).map((k) => {
      if ((posts[k].word && posts[k].word.toLocaleLowerCase().match(re)) || !re) {
        matchPostsKeysCopy.push(posts[k].word);
      }
    });

    this.setState({ matchPostsKeys:matchPostsKeysCopy });
    if (e.target.value.length > 0 && matchPostsKeysCopy.length !== 0){
      this.setState({ showDropList:true });
    } else {
      this.setState({ showDropList:false });
    }
  }
  
  changePage = (page) => {
    this.setState({ currentPage:page.selected });
  }

  checkAuth() {
    if (!this.props.userData.auth) this.props.history.push('/login');
  }

  updateListLevel(lvl) {
    this.props.setLevel(lvl);
    this.loadPosts(this.props.userData.currentLangs, lvl);
  }

  loadPosts(langs, level) {
    this.props.clearPost();
    this.props.loadPosts(langs, level);
  }

  updatePostsList() {
    this.props.updatePosts(this.props.editedPosts);
  }

  addPost() {
    const data = {
      word: '',
      trnaslate: '',
      phrases: [{ text: '', translate: '' }],
      answers: [''],
      langs: this.props.userData.currentLangs,
      level: this.props.userData.level,
    };

    this.props.addPost(data);
  }

  sortPosts(matchText) {
    const postsList = {};
    const { posts } = this.props;
    const currentPage = this.state.currentPage > 0 ? this.state.currentPage : 0;
    const re = matchText.toLocaleLowerCase() || '';

    if (Object.keys(posts).length > 0) {
      Object.keys(posts).map((k) => {
        if ((posts[k].word && posts[k].word.toLocaleLowerCase().match(re)) || !re){
          postsList[k] = posts[k];
        }
      });
    }

    const sortedListKeys = Object.keys(postsList).slice(currentPage * 5, ((currentPage + 1) * 5));
    const sortedList = {};
    sortedListKeys.map((k) => {
      sortedList[k] = postsList[k];
    });
    
    return { postsList, sortedList };
  }

  render() {
    const sortedPosts = this.sortPosts(this.state.searchQuery);

    const { userData } = this.props;
    const { showDropList, matchPostsKeys } = this.state;
    const { postsList, sortedList } = this.sortPosts(this.state.searchQuery);
    const pageCount = this.getPageCount(postsList);

    return (
      <div className={`${mStyles.content} ${styles.content}`}>
        <div className={mStyles.row}>
          <div className={styles.controller}>
            <div className={mStyles.col6}>
              <button className={`${mStyles.tag} ${mStyles.tagActive}`}>Лента</button>
            </div>
            <div className={mStyles.col6}>
              <div className={`${mStyles.col6} ${mStyles.tARight}`}>
                <div className={`${mStyles.searchFieldWrap} ${showDropList && mStyles.searchFieldWrapDropListOpen }`}>
                  <input type="text" placeholder="Search" onKeyUp={(e) => { this.setSearchQuery(e); }} onChange={(e) => { this.openDropList(e); }} className={mStyles.searchField} />
                  <svg width="18" height="18" viewBox="0 0 18 18">
                    <g fill="none">
                      <path fill="#6B758E" d="M11.625 10.5h-.592l-.21-.203A4.853 4.853 0 0 0 12 7.125 4.875 4.875 0 1 0 7.125 12a4.853 4.853 0 0 0 3.172-1.178l.203.21v.593l3.75 3.742 1.117-1.117-3.742-3.75zm-4.5 0A3.37 3.37 0 0 1 3.75 7.125 3.37 3.37 0 0 1 7.125 3.75 3.37 3.37 0 0 1 10.5 7.125 3.37 3.37 0 0 1 7.125 10.5z" />
                    </g>
                  </svg>
                  <ul className={mStyles.searchFieldWrapDropList}>
                    {
                      matchPostsKeys.map((k) => (
                        <li key={`match${k.replace(' ', '')}`}><button onClick={(e) => { this.setSearchQuery(e, true); }} value={k}>{k}</button></li>
                      ))
                    }
                  </ul>
                </div>
              </div>
              <div className={`${mStyles.col6} ${mStyles.tARight}`}>
                <button onClick={() => this.addPost()} className={`${mStyles.btn} ${mStyles.btnBlue}`}>Добавить пост</button>
              </div>
            </div>
          </div>
          <div className={mStyles.col12}>
            {(Object.keys(sortedList).length === 0)
              ? (<div className={mStyles.preloader}><div className={mStyles.logo} /><div className={mStyles.loader} /></div>)
              : (
                <div>
                  <div className={`${mStyles.col6} ${styles.blockWrap}`}>
                    {
                      Object.keys(sortedList).slice(0).reverse().map((item, indx, arr) => {
                        if (indx % 2 === 0) {
                          return (
                            <PostItem
                              activeItem={this.state.activeItem}
                              key={`PostItem${(item + 1)}`}
                              postData={sortedList[item]}
                              indx={arr.length - 1 - indx}
                              postIndx={+item + 1}
                              blockpos="left"
                            />);
                        }
                      })
                    }
                  </div>
                  <div className={`${mStyles.col6} ${styles.blockWrap}`}>
                    {
                      Object.keys(sortedList).slice(0).reverse().map((item, indx, arr) => {
                        if (indx % 2 > 0) {
                          return (
                            <PostItem
                              activeItem={this.state.activeItem}
                              key={`PostItem${(item + 1)}`}
                              postData={sortedList[item]}
                              indx={arr.length - 1 - indx}
                              postIndx={+item + 1}
                              blockpos="right"
                            />);
                        }
                      })
                    }
                  </div>
                </div>
              )
            }
          </div>
          <div className={`${mStyles.col12} ${styles.pagination}`}>
            <ReactPaginate
              breakLabel={<span>...</span>}
              pageCount={pageCount}
              pageRangeDisplayed={2}
              forcePage={this.state.currentPage}
              activeClassName={styles.paginationActive}
              initialPage={0}
              previousClassName={styles.paginationDisable}
              nextClassName={styles.paginationDisable}
              onPageChange={(page) => this.changePage(page)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({ userData:state.user, posts:state.posts }),
  dispatch => bindActionCreators({ loadPosts, addPost, setLevel, getLangs, setLangs, clearPost, updatePosts }, dispatch),
)(AdminFeed);
