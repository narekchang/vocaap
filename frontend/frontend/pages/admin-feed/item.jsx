// @flow
import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, TextArea } from "react-form";
import autosize from "autosize";

import { delPost, loadPosts, updatePost } from './redux/actions';

import PhrasesItem from './pharses';
import UploadComponent from './uploadFile';
import SelectLevel from './select';

import ms from '../../style.styl';
import styles from './style.styl';

class PostItem extends React.Component<{
  blockpos: string,
  userData: Object,
  postData: Object,
  delPost: Object,
  loadPosts: Object,
  updatePost: Object,
  postIndx: Number,
}> {

  componentWillMount() {
    const { postData } = this.props;
    const { itemData } = this;

    itemData.word = postData.word || '';
    itemData.explanation = postData.explanation || '';
    itemData.media = postData.media || '';
    itemData.anchor = postData.anchor || '';
    itemData.level = postData.level || [ 0, 1, 2 ];
  }

  componentDidMount() {
    const textareas = document.getElementsByTagName('textarea');
    autosize(textareas);
  }

  setCaretPosition = (elemId, caretPos) => {
    const elem = document.getElementById(elemId);

    if (elem != null) {
      if (elem.createTextRange) {
        const range = elem.createTextRange();
        range.move('character', caretPos);
        range.select();
      }
      else if (elem.selectionStart) {
        elem.focus();
        elem.setSelectionRange(caretPos, caretPos);
      }
      else
        elem.focus();
    }
  }

  doGetCaretPosition = (oField) => {
    let iCaretPos = 0;

    if (document.selection) {
      oField.focus();

      const oSel = document.selection.createRange();

      oSel.moveStart('character', -oField.value.length);
      iCaretPos = oSel.text.length;
    }

    else if (oField.selectionStart || oField.selectionStart === '0') iCaretPos = oField.selectionStart;

    return iCaretPos;
  }

  del() {
    const { postData, userData } = this.props;
    this.props.delPost(postData._id).then(() => { this.props.loadPosts(userData.currentLangs, (userData.level || 0)) });
  }
  
  reqDel() {
    this.deleting = !this.deleting;
  }

  editProps(values) {
    const { word, explanation, media, anchor, levelStr } = values;
    const { itemData } = this;

    const levelStrng =
      levelStr.toString()[levelStr.toString().length - 1] === "," || levelStr.toString()[levelStr.toString().length - 1] === " "
        ? levelStr.toString().slice(0, levelStr.toString().length - 1)
        : levelStr.toString();

    itemData.word = word || '';
    itemData.explanation = explanation || '';
    itemData.media = media || '';
    itemData.anchor = anchor || '';
    itemData.level = levelStr ? levelStrng.replace(/, /g, ',').split(",") : [''];
    this.update();
  }

  update() {
    const { postData, postIndx } = this.props;
    const { itemData } = this;

    this.props.updatePost(postData._id, itemData, (postIndx - 1));
  }

  itemData = {};
  deleting = false;

  render() {
    const { blockpos, postIndx, postData } = this.props;
    const { word, explanation, media, anchor, level } = postData;
    const levelStr = level ? level.toString() : '';

    return (
      <div className={`${ms.block} ${ms.blockDay} ${ms.col12} ${styles.block}`}>
        <Form
          className={ms.blockBody}
          onSubmit={values => this.editProps(values)}
          values={{ word, explanation, media, anchor, levelStr }}
          render={({ submitForm, setValue, values }) => {
            const selectEm = (em, fieldId) => {
              const oField = document.getElementById(fieldId);
              const cursorPos = this.doGetCaretPosition(oField);

              setValue('word', `${values.word.slice(0, cursorPos)}${em}${values.word.slice(cursorPos)}`);
              submitForm().then(() => this.setCaretPosition(fieldId, (cursorPos + em.length)));
            };
            return (
              <form onSubmit={submitForm}>
                <div className={ms.blockTitle}>
                  Post {postIndx}
                  {(this.deleting)
                    ? (
                      <div className={styles.delete}>
                        <span>Delete?</span>
                        <button onClick={() => this.del()} className={styles.deleteBtn}>Yes</button>
                        <button onClick={() => this.reqDel()} className={styles.deleteBtn}>Nope</button>
                      </div>
                    )
                    : (<button onClick={() => this.reqDel()} className={styles.deleteIc} />)
                  }
                </div>
                <div className={ms.blockBodyItemsWrap}>
                  <div className={ms.blockBodyItemPhrase}>
                    <div className={ms.blockBodyItem}>
                      <div className={ms.leftBlock}>Описание</div>
                      <div className={ms.rightBlock}>
                        <div
                          className={`${styles.inputEmojiPicker} ${blockpos}`}
                          onMouseLeave={() => setValue(`emojiWindow`, false)}
                        >
                          <TextArea onBlur={() => submitForm()} className={`${ms.field} ${ms.fieldEmoji} ${ms.col12}`} id={`field${postIndx * 93}`} field="word" placeholder="In English with emoji" />

                          <PhrasesItem
                            emojiWindow={values.emojiWindow}
                            handle={() => setValue(`emojiWindow`, true)}
                            select={(em) => selectEm(em, `field${postIndx * 93}`)}
                            blockpos={blockpos}
                          />
                        </div>
                      </div>
                    </div>
                    <div className={ms.blockBodyItem}>
                      <div className={ms.leftBlock}>Пояснение</div>
                      <div className={ms.rightBlock}>
                        <TextArea onBlur={() => submitForm()} className={`${ms.field} ${ms.col12}`} field="explanation" placeholder="На русском" />
                      </div>
                    </div>
                  </div>
                </div>
                <UploadComponent field='media' val={media} />
                <div className={ms.blockBodyItemsWrap}>
                  <div className={ms.blockBodyItem}>
                    <div className={ms.leftBlock}>Привязка</div>
                    <div className={ms.rightBlock}>
                      <TextArea onBlur={() => submitForm()} className={`${ms.field} ${ms.col12}`} field="anchor" placeholder="In English" />
                    </div>
                  </div>
                  <div className={ms.blockBodyItem}>
                    <div className={ms.leftBlock}>Уровень</div>
                    <div className={ms.rightBlock}>
                      <SelectLevel field="media" val={level} />
                    </div>
                  </div>
                </div>
              </form>
            )}
          }
        />
      </div>
    );
  }
}

export default connect(
  state => ({ userData:state.user }),
  dispatch => bindActionCreators({ delPost, loadPosts, updatePost }, dispatch),
)(PostItem);