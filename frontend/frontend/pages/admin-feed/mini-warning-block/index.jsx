// @flow
import React from 'react';
import { branch, renderNothing, pure, mapProps, compose, type HOC } from 'recompose';
import styles from './style.styl';

type OuterProps = {|
  title: string,
  desc: string,  
  condition: boolean,
|};
type EnhancedProps = {|
  title: string,
  desc: string,  
|};

const WarningBlockMini = ({
  title,
  desc,
}: EnhancedProps) => (
  <div className={styles.warningBlock}>
    <div className={styles.warningBlockTitle}>{title}</div>
    {desc}
  </div>
);

const enhance: HOC<EnhancedProps, OuterProps> = compose(
  pure,
  branch(
    ({ condition }: OuterProps) => !condition,
    renderNothing,
    // pure,
  ),
  mapProps(({ title, desc }: OuterProps): EnhancedProps => ({ title, desc })),
);

export default enhance(WarningBlockMini);
