// @flow
import { getPosts, createPost, updatePostData, deletePost } from '_api/posts';

type LoadPostsAction = {
  type: "LOAD_POSTS",
  posts: Object,
};
type UpdatePostAction = {
  type: 'UPDATE_POST',
  posts: Object,
};
type UpdatePostsAction = {
  type: 'UPDATE_POSTS',
  posts: Object,
};
type EditPostAction = {
  type: 'EDIT_POST',
  posts: Object,
};
type CreatePostAction = {
  type: 'CREATE_POST',
  posts: Object,
};
type DeletePostAction = {
  type: 'CREATE_POST',
};
type ClearPostAction = {
  type: 'CLEAR_POSTS',
};

export type PostsAction =
  | LoadPostsAction
  | UpdatePostAction
  | UpdatePostsAction
  | EditPostAction
  | CreatePostAction
  | DeletePostAction
  | ClearPostAction
  ;

type LoadPostsCreator = () => LoadPostsAction;
export const loadPosts: LoadPostsCreator = (langs, level) => (dispatch) => new Promise((resolve) => {

  getPosts(langs, level).then((posts) => {
    dispatch({
      type: "LOAD_POSTS",
      posts,
    });
  
    resolve(posts);
  });
});

type CreatePostCreator = () => CreatePostAction;
export const addPost: CreatePostCreator = (langs, data) => (dispatch) => new Promise((resolve) => {

  createPost(langs, data).then((post) => {
    dispatch({
      type: "CREATE_POST",
      post,
    });
  
    resolve(post);
  });
});

type DeletePostCreator = () => DeletePostAction;
export const delPost: DeletePostCreator = (id) => (dispatch) => new Promise((resolve) => {

  deletePost(id).then((post) => {
    dispatch({
      type: "DELETE_POST",
    });
  
    resolve(post);
  });
});

type UpdatePostCreator = () => UpdatePostAction;
export const updatePost: UpdatePostCreator = (id, newData, postIndex) => (dispatch) => new Promise((resolve) => {
  const editedPosts = {};
  editedPosts[postIndex] = { ...newData, _id:id };

  updatePostData(id, newData).then((post) => {
    dispatch({
      type: "UPDATE_POST",
      editedPosts,
    });
  
    resolve(post);
  });
});

type UpdatePostsCreator = () => UpdatePostsAction;
export const updatePosts: UpdatePostsCreator = (posts) => (dispatch) => {
  dispatch({
    type: "UPDATE_POSTS",
    posts,
  });
};

type EditPostCreator = () => EditPostAction;
export const editPost: EditPostCreator = (posts) => (dispatch) => {
  dispatch({
    type: "EDIT_POST",
    posts,
  });
};

type ClearPostCreator = () => ClearPostAction;
export const clearPost: ClearPostCreator = () => (dispatch) => {
  dispatch({
    type: "CLEAR_POSTS",
    posts: {},
  });
};