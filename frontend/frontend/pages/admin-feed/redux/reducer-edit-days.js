// @flow
import type { Action } from '_state/actionTypes';

type State = {};
const initial = {};

type Reducer = (state: State, action: Action) => State;
const reducer: Reducer = (state = initial, action) => {
  switch (action.type) {
  case 'LOAD_POSTS': {
    return {
      ...action.posts,
    };
  }
  case 'DELETE_POST': {
    return {
      ...state,
    };
  }
  case 'UPDATE_POST': {
    return {
      ...state,
      ...action.editedDays,
    };
  }
  default:
    return state;
  }
};

export default reducer;