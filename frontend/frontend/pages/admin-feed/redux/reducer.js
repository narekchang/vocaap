// @flow
import type { Action } from '_state/actionTypes';

type State = {};
const initial = {};

type Reducer = (state: State, action: Action) => State;
const reducer: Reducer = (state = initial, action) => {
  switch (action.type) {
  case 'LOAD_POSTS': {
    return {
      ...action.posts,
    };
  }
  case 'UPDATE_POSTS': {
    return {
      ...state,
      ...action.posts,
    };
  }
  case 'DELETE_POST': {
    return {
      ...state,
    };
  }
  case 'EDIT_POST': {
    return {
      ...state,
      ...action.posts,
    };
  }
  case 'CLEAR_POSTS': {
    return {
      ...action.posts,
    };
  }
  case 'CREATE_POST': {
    const day = {};
    day[Object.keys(state).length] = action.day;

    return {
      ...state,
      ...day,
    };
  }
  default:
    return state;
  }
};

export default reducer;