// @flow
import React from 'react';
import onClickOutside from "react-onclickoutside";

import ms from '../../style.styl';
import s from './style.styl';

class Select extends React.Component<{
  field: String,
  val: Object,
}> {
  constructor(props) {
    super(props);
    this.state = {
      selectVal: [ 0, 1, 2 ],
      showList: false,
    };
  }

  componentWillMount() {
    this.setState({ selectVal:this.props.val });
  }

  onSelect(val) {
    const { selectVal } = this.state;
    let newVal = selectVal;
    const selectValStr = selectVal.sort().toString();

    if (selectValStr === '0,1,2') {
      newVal = [val];
    } else if (selectValStr.indexOf(val) > -1) {
      newVal = [];
      selectVal.map(item => {
        if (item !== val || selectValStr.length < 2) newVal.push(item);
      });
    } else {
      newVal.push(val);
    }

    this.setState({ selectVal:newVal });
  }

  onSelectAll() {
    this.setState({ selectVal:[ 0, 1, 2 ]});
  }

  toggleList() {
    this.setState({ showList:!this.state.showList });
  }

  handleClickOutside() {
    this.setState({ showList:false });
  }
  
  render() {
    const { selectVal, showList } = this.state;
    let selectLabel = '';
    const labels = [ 'Базовый', 'Средний', 'Продвинутый', 'Для всех уровней' ];
    console.log(this.state)

    if (selectVal.sort().toString() === '0,1,2') {
      selectLabel = labels[3];
    } else {
      selectVal.map(item => {
        selectLabel += (selectLabel.length > 0 ? ', ' : '') + labels[item];
      });
    }
    
    return (
      <div className={ms.selectWrap}>
        <button onClick={() => this.toggleList()} className={ms.selected}>
          {selectLabel}
        </button>
        {
          showList && (
            <div className={ms.selectList}>
              <button className={`${ms.select} ${selectVal.sort().toString() === '0,1,2' && ms.selectActive}`} onClick={() => this.onSelectAll()}>
                Для всех уровней
              </button>
              <button className={`${ms.select} ${selectVal.indexOf(0) > -1 && ms.selectActive}`} onClick={() => this.onSelect(0)}>
                Базовый
              </button>
              <button className={`${ms.select} ${selectVal.indexOf(1) > -1 && ms.selectActive}`} onClick={() => this.onSelect(1)}>
                Средний
              </button>
              <button className={`${ms.select} ${selectVal.indexOf(2) > -1 && ms.selectActive}`} onClick={() => this.onSelect(2)}>
                Продвинутый
              </button>
            </div>
          )
        }
      </div>
    );
  }
}

export default onClickOutside(Select);