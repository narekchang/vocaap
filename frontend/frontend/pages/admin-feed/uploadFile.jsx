import React from 'react';
import ms from '../../style.styl';
import s from './style.styl';

export default class UploadComponent extends React.Component<{
  field: String,
  val: string,
}> {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      previewUrl: this.props.val || '',
    };
    this._handleImageChange = this._handleImageChange.bind(this);
  }

  componentWillMount() {
    console.log('this.props.field', this.props.val)
  }

  _handleImageChange(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        previewUrl: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  removeMedia() {
    this.setState({ previewUrl:null });
  }

  render() {
    const { previewUrl, file } = this.state;
    const { name, type } = file;
    let $preview = null;

    if (previewUrl) {
      $preview = (
        <div className={`${ms.previewWrap} ${(type && type.indexOf('video') > -1 || !type && previewUrl.indexOf('mp4') > -1) && ms.video}`}>
          <button className={ms.del} onClick={() => this.removeMedia()} />
          {
            (type && type.indexOf('video') > -1 || !type && previewUrl.indexOf('mp4') > -1)
              ? (
                <video width="480" height="280" controls>
                  <track kind="captions" />
                  <source src={previewUrl} type={type} />
                </video>
              )
              : (<img src={previewUrl} alt={name} />)
          }
        </div>
      );
    }
    

    return (
      <div className={ms.blockBodyItemsWrap}>
        {
          ($preview)
            ? ($preview)
            : (
              <div className={ms.blockBodyItem}>
                <div className={ms.leftBlock} />
                <div className={ms.rightBlock}>
                  <label className={`${ms.btn} ${ms.btnBorder}`}>
                    Загрузить файл
                    <input type="file" onChange={this._handleImageChange} accept="video/mp4,video/x-m4v,video/*,image/x-png,image/gif,image/jpeg,image/*" hidden />
                  </label>
                </div>
              </div>)
        }
      </div>
    )
  }
}