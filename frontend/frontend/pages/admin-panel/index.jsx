// @flow
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactPaginate from 'react-paginate';

import DayItem from './item';

import { loadDays, addDay, clearDays, updateDays } from './redux/actions';
import { setLevel, setLangs, getLangs } from '../login/redux/actions';

import mStyles from '../../style.styl';
import styles from './style.styl';

class AdminPanel extends React.Component<{
  history: Object,
  userData: Object,
  days: Object,
  editedDays: Object,
  addDay: Object,
  updateDays: Object,
  loadDays: Object,
  setLevel: Object,
  setLangs: Object,
  getLangs: Object,
  clearDays: Object,
}> {
  constructor(props){
    super(props);

    this.state = {
      activeItem: 0,
      searchQuery: '',
      currentPage: 1,
      showDropList: false,
      matchDaysKeys: [],
    };
  }

  componentWillMount() {
    this.checkAuth();
    this.loadDays(this.props.userData.currentLangs, (this.props.userData.level || 0));
    
    const { userData, setLangs, getLangs } = this.props;
    if (userData.auth){
      if (userData.langs[0] === 'all' || userData.currentLangs === 'all'){
        getLangs().then((langs) => setLangs(langs[0]));
      } else {
        setLangs(userData.currentLangs || userData.langs[0]);
      }
    }
  }
  
  setActiveItem(item) {
    this.setState({
      activeItem: item,
    });
  }

  setSearchQuery(e, notKeyPress) {
    const { matchDaysKeys } = this.state;
    
    if (((notKeyPress || e.keyCode === 13) && matchDaysKeys.length > 0) || e.target.value.length === 0) {
      this.setState({ currentPage:0 });
      this.setState({ searchQuery:e.target.value });
      this.setState({ showDropList:false });
    }
  }
  
  getPageCount = (arr) => {
    const arrLength = Object.keys(arr).length;
    let pageCount = Math.trunc(arrLength / 5);
    
    pageCount = arrLength % 5 > 0 ? pageCount + 1 : pageCount;
    
    return pageCount;
  }
  
  openDropList(e) {
    const { days } = this.props;
    const matchDaysKeysCopy = [];
    const re = e.target.value.toLocaleLowerCase();

    Object.keys(days).map((k) => {
      if ((days[k].word && days[k].word.toLocaleLowerCase().match(re)) || !re) {
        matchDaysKeysCopy.push(days[k].word);
      }
    });

    this.setState({ matchDaysKeys:matchDaysKeysCopy });
    if (e.target.value.length > 0 && matchDaysKeysCopy.length !== 0){
      this.setState({ showDropList:true });
    } else {
      this.setState({ showDropList:false });
    }
  }
  
  changePage = (page) => {
    this.setState({ currentPage:page.selected });
  }

  checkAuth() {
    if (!this.props.userData.auth) this.props.history.push('/login');
  }

  updateListLevel(lvl) {
    this.props.setLevel(lvl);
    this.loadDays(this.props.userData.currentLangs, lvl);
  }

  loadDays(langs, level) {
    this.props.clearDays();
    this.props.loadDays(langs, level);
  }

  updateDaysList() {
    this.props.updateDays(this.props.editedDays);
  }

  addDay() {
    const data = {
      word: '',
      trnaslate: '',
      phrases: [{ text:'', translate:'' }],
      answers: [''],
      langs: this.props.userData.currentLangs,
      level: this.props.userData.level,
    };

    this.props.addDay(data);
  }

  sortDays(matchText) {
    const daysList = {};
    const { days } = this.props;
    const currentPage = this.state.currentPage > 0 ? this.state.currentPage : 0;
    const re = matchText.toLocaleLowerCase() || '';
    
    if (Object.keys(days).length > 0) {
      Object.keys(days).map((k) => {
        if ((days[k].word && days[k].word.toLocaleLowerCase().match(re)) || !re){
          daysList[k] = days[k];
        }
      });
    }

    const sortedListKeys = Object.keys(daysList).slice(currentPage * 5, ((currentPage + 1) * 5));
    const sortedList = {};
    sortedListKeys.map((k) => {
      sortedList[k] = daysList[k];
    });
    
    return { daysList, sortedList };
  }

  render() {
    const { userData } = this.props;
    const { showDropList, matchDaysKeys } = this.state;
    const { daysList, sortedList } = this.sortDays(this.state.searchQuery);
    const pageCount = this.getPageCount(daysList);

    return (
      <div className={`${mStyles.content} ${styles.content}`}>
        <div className={mStyles.row}>
          <div className={styles.controller}>
            <div className={mStyles.col6}>
              <button
                onClick={() => this.updateListLevel(0)}
                className={`${mStyles.tag} ${!userData.level && mStyles.tagActive}`}
              >
                Базовый
              </button>
              <button
                onClick={() => this.updateListLevel(1)}
                className={`${mStyles.tag} ${userData.level === 1 && mStyles.tagActive}`}
              >
                Средний
              </button>
              <button
                onClick={() => this.updateListLevel(2)}
                className={`${mStyles.tag} ${userData.level === 2 && mStyles.tagActive}`}
              >
                Продвинутый
              </button>
            </div>
            <div className={mStyles.col6}>
              <div className={`${mStyles.col6} ${mStyles.tARight}`}>
                <div className={`${mStyles.searchFieldWrap} ${showDropList && mStyles.searchFieldWrapDropListOpen }`}>
                  <input type="text" placeholder="Search" onKeyUp={(e) => { this.setSearchQuery(e); }} onChange={(e) => { this.openDropList(e); }} className={mStyles.searchField} />
                  <svg width="18" height="18" viewBox="0 0 18 18">
                    <g fill="none">
                      <path fill="#6B758E" d="M11.625 10.5h-.592l-.21-.203A4.853 4.853 0 0 0 12 7.125 4.875 4.875 0 1 0 7.125 12a4.853 4.853 0 0 0 3.172-1.178l.203.21v.593l3.75 3.742 1.117-1.117-3.742-3.75zm-4.5 0A3.37 3.37 0 0 1 3.75 7.125 3.37 3.37 0 0 1 7.125 3.75 3.37 3.37 0 0 1 10.5 7.125 3.37 3.37 0 0 1 7.125 10.5z" />
                    </g>
                  </svg>
                  <ul className={mStyles.searchFieldWrapDropList}>
                    {
                      matchDaysKeys.map((k) => (
                        <li key={`match${k.replace(' ', '')}`}><button onClick={(e) => { this.setSearchQuery(e, true); }} value={k}>{k}</button></li>
                      ))
                    }
                  </ul>
                </div>
              </div>
              <div className={`${mStyles.col6} ${mStyles.tARight}`}>
                <button onClick={() => this.addDay()} className={`${mStyles.btn} ${mStyles.btnBlue}`}>Добавить день</button>
              </div>
            </div>
          </div>
          <div className={mStyles.col12}>
            {(Object.keys(sortedList).length === 0)
              ? (<div className={mStyles.preloader}><div className={mStyles.logo} /><div className={mStyles.loader} /></div>)
              : (
                <div>
                  <div className={`${mStyles.col6} ${styles.blockWrap}`}>
                    {
                      Object.keys(sortedList).slice(0).reverse().map((item, indx, arr) => {
                        if (indx % 2 === 0){
                          return (
                            <DayItem
                              activeItem={this.state.activeItem}
                              key={`DayItem${(item + 1)}`}
                              dayData={sortedList[item]}
                              indx={arr.length - 1 - indx}
                              dayIndx={+item + 1}
                              blockpos="left"
                            />);
                        }
                      })
                    }
                  </div>
                  <div className={`${mStyles.col6} ${styles.blockWrap}`}>
                    {
                      Object.keys(sortedList).slice(0).reverse().map((item, indx, arr) => {
                        if (indx % 2 > 0){
                          return (
                            <DayItem
                              activeItem={this.state.activeItem}
                              key={`DayItem${(item + 1)}`}
                              dayData={sortedList[item]}
                              indx={arr.length - 1 - indx}
                              dayIndx={+item + 1}
                              blockpos="right"
                            />);
                        }
                      })
                    }
                  </div>
                </div>
              )
            }
          </div>
          <div className={`${mStyles.col12} ${styles.pagination}`}>
            <ReactPaginate
              breakLabel={<span>...</span>}
              pageCount={pageCount}
              pageRangeDisplayed={2}
              forcePage={this.state.currentPage}
              activeClassName={styles.paginationActive}
              initialPage={0}
              previousClassName={styles.paginationDisable}
              nextClassName={styles.paginationDisable}
              onPageChange={(page) => this.changePage(page)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({ userData:state.user, days:state.days, editedDays:state.editedDays }),
  dispatch => bindActionCreators({ loadDays, addDay, setLevel, getLangs, setLangs, clearDays, updateDays }, dispatch),
)(AdminPanel);
