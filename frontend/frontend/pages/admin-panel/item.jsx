// @flow
import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, NestedField, TextArea } from "react-form";
import autosize from "autosize";

import { delDay, loadDays, updateDay } from './redux/actions';

import PhrasesItem from './pharses';

import mStyles from '../../style.styl';
import styles from './style.styl';

class DayItem extends React.Component<{
  blockpos: string,
  userData: Object,
  dayData: Object,
  delDay: Object,
  loadDays: Object,
  updateDay: Object,
  dayIndx: Number,
}> {

  componentWillMount() {
    const { dayData } = this.props;
    const { itemData } = this;

    itemData.word = dayData.word || '';
    itemData.translate = dayData.translate || '';
    itemData.phrases = dayData.phrases || [{ text:'', translate:'' }];
    itemData.answers = dayData.answers || [''];
  }

  componentDidMount() {
    const textareas = document.getElementsByTagName('textarea');
    autosize(textareas);
  }

  setCaretPosition = (elemId, caretPos) => {
    const elem = document.getElementById(elemId);

    if (elem != null) {
      if (elem.createTextRange) {
        const range = elem.createTextRange();
        range.move('character', caretPos);
        range.select();
      }
      else if (elem.selectionStart) {
        elem.focus();
        elem.setSelectionRange(caretPos, caretPos);
      }
      else
        elem.focus();
    }
  }

  doGetCaretPosition = (oField) => {
    let iCaretPos = 0;

    if (document.selection) {
      oField.focus();

      const oSel = document.selection.createRange();

      oSel.moveStart('character', -oField.value.length);
      iCaretPos = oSel.text.length;
    }

    else if (oField.selectionStart || oField.selectionStart === '0') iCaretPos = oField.selectionStart;

    return iCaretPos;
  }

  del() {
    const { dayData, userData } = this.props;
    this.props.delDay(dayData._id).then(() => { this.props.loadDays(userData.currentLangs, (userData.level || 0)) });
  }
  
  reqDel() {
    this.deleting = !this.deleting;
  }

  editProps(values) {
    const { word, translate, answersStr, phrases } = values;
    const { itemData } = this;

    const answersStrng =
      answersStr.toString()[answersStr.toString().length - 1] === "," || answersStr.toString()[answersStr.toString().length - 1] === " "
        ? answersStr.toString().slice(0, answersStr.toString().length - 1)
        : answersStr.toString();

    itemData.word = word || '';
    itemData.translate = translate || '';
    itemData.phrases = [];
    itemData.answers = answersStr ? answersStrng.replace(/, /g, ',').split(",") : [''];

    (phrases || [{}]).map(item => {
      const p = {};
      p.text = item.text || '';
      p.translate = item.translate || '';

      itemData.phrases.push(p);
    });

    this.update();
  }

  update() {
    const { dayData, dayIndx } = this.props;
    const { itemData } = this;

    this.props.updateDay(dayData._id, itemData, (dayIndx - 1));
  }

  itemData = {};
  deleting = false;

  render() {
    const { blockpos, dayIndx, dayData } = this.props;
    const { word, translate, answers } = dayData;
    const answersStr = answers ? answers.toString() : '';
    const { phrases } = this.itemData;

    return (
      <div className={`${mStyles.block} ${mStyles.blockDay} ${mStyles.col12} ${styles.block}`}>
        <Form
          className={mStyles.blockBody}
          onSubmit={values => this.editProps(values)}
          values={{ phrases, word, translate, answersStr }}
          render={({ submitForm, setValue, values, addValue }) => (
            <form onSubmit={submitForm}>
              <div className={mStyles.blockTitle}>
                DAY {dayIndx}
                {(this.deleting)
                  ? (
                    <div className={styles.delete}>
                      <span>Delete?</span>
                      <button onClick={() => this.del()} className={styles.deleteBtn}>Yes</button>
                      <button onClick={() => this.reqDel()} className={styles.deleteBtn}>Nope</button>
                    </div>
                  )
                  : (<button onClick={() => this.reqDel()} className={styles.deleteIc} />)
                }
              </div>
              <div className={mStyles.blockBodyItemsWrap}>
                <div className={mStyles.blockBodyItem}>
                  <div className={mStyles.leftBlock}>Слово дня</div>
                  <div className={mStyles.rightBlock}>
                    <TextArea onBlur={() => submitForm()} className={`${mStyles.field} ${mStyles.col12}`} field="word" placeholder="In English" />
                  </div>
                </div>
                <div className={mStyles.blockBodyItem}>
                  <div className={mStyles.leftBlock}>Перевод</div>
                  <div className={mStyles.rightBlock}>
                    <TextArea onBlur={() => submitForm()} className={`${mStyles.field} ${mStyles.col12}`} field="translate" placeholder="На русском" />
                  </div>
                </div>
              </div>
              <div className={mStyles.blockBodyItemsWrap}>
                <NestedField
                  field='phrases'
                  render={() => (
                    <div>
                      {values.phrases &&
                        Object.keys(values.phrases).map((item, i) => (
                          <div key={(i + 1) * dayIndx}>
                            <NestedField
                              field={i}
                              render={() => {
                                const selectEm = (em, fieldId) => {
                                  const oField = document.getElementById(fieldId);
                                  const cursorPos = this.doGetCaretPosition(oField);

                                  setValue(`phrases[${i}].text`, `${values.phrases[i].text.slice(0, cursorPos)}${em}${values.phrases[i].text.slice(cursorPos)}`);
                                  submitForm().then(() => this.setCaretPosition(fieldId, (cursorPos + em.length)));
                                };

                                return (
                                  <div className={mStyles.blockBodyItemPhrase}>
                                    <div className={mStyles.blockBodyItem}>
                                      <div className={mStyles.leftBlock}>Фраза #{i + 1}</div>
                                      <div className={mStyles.rightBlock}>
                                        <div
                                          className={`${styles.inputEmojiPicker} ${blockpos}`}
                                          onMouseLeave={() => setValue(`phrases[${i}].emojiWindow`, false)}
                                        >
                                          <TextArea onBlur={() => submitForm()} className={`${mStyles.field} ${mStyles.fieldEmoji} ${mStyles.col12}`} id={`field${(i + 1) * dayIndx * 92}`} field="text" placeholder="In English with emoji" />

                                          <PhrasesItem
                                            emojiWindow={values.phrases[i].emojiWindow}
                                            handle={() => setValue(`phrases[${i}].emojiWindow`, true)}
                                            select={(em) => selectEm(em, `field${(i + 1) * dayIndx * 92}`)}
                                            blockpos={blockpos}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className={mStyles.blockBodyItem}>
                                      <div className={mStyles.leftBlock}>Перевод #{i + 1}</div>
                                      <div className={mStyles.rightBlock}>
                                        <TextArea onBlur={() => submitForm()} onLoad={() => autosize(document.getElementsByTagName('textarea'))} className={`${mStyles.field} ${mStyles.col12}`} field="translate" placeholder="На русском" />
                                      </div>
                                    </div>
                                  </div>
                                )
                              }}
                            />
                          </div>)
                        )}
                      <div className={mStyles.blockBodyItem}>
                        <div className={mStyles.leftBlock} />
                        <div className={mStyles.rightBlock}>
                          <button
                            type="button"
                            onClick={() => { addValue('phrases', {}); setTimeout(() => autosize(document.getElementsByTagName('textarea')), 10);}}
                            className={`${mStyles.btn} ${mStyles.btnBorder} ${styles.addPhraseBtn}`}
                          >
                            Добавить фразу
                          </button>
                        </div>
                      </div>
                    </div>
                  )}
                />
              </div>
              <div className={mStyles.blockBodyItemsWrap}>
                <div className={mStyles.blockBodyItem}>
                  <div className={mStyles.leftBlock}>Для теста</div>
                  <div className={mStyles.rightBlock}>
                    <TextArea onBlur={() => submitForm()} className={`${mStyles.field} ${mStyles.col12}`} field="answersStr" placeholder="Три слова через запятую" />
                  </div>
                </div>
              </div>
            </form>
          )}
        />
      </div>
    );
  }
}

export default connect(
  state => ({ userData:state.user }),
  dispatch => bindActionCreators({ delDay, loadDays, updateDay }, dispatch),
)(DayItem);