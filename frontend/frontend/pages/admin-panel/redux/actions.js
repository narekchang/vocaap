// @flow
import { getDays, createDay, updateDayData, deleteDay } from '_api/days';

type LoadDaysAction = {
  type: "LOAD_DAYS",
  days: Object,
};
type UpdateDayAction = {
  type: 'UPDATE_DAY',
  day: Object,
};
type UpdateDaysAction = {
  type: 'UPDATE_DAYS',
  days: Object,
};
type EditDayAction = {
  type: 'EDIT_DAY',
  days: Object,
};
type CreateDayAction = {
  type: 'CREATE_DAY',
  days: Object,
};
type DeleteDayAction = {
  type: 'CREATE_DAY',
};
type ClearDaysAction = {
  type: 'CLEAR_DAYS',
};

export type DaysAction =
  | LoadDaysAction
  | UpdateDayAction
  | EditDayAction
  | CreateDayAction
  | DeleteDayAction
  ;

type LoadDaysCreator = () => LoadDaysAction;
export const loadDays: LoadDaysCreator = (langs, level) => (dispatch) => new Promise((resolve) => {

  getDays(langs, level).then((days) => {
    dispatch({
      type: "LOAD_DAYS",
      days,
    });
  
    resolve(days);
  });
});

type CreateDayCreator = () => CreateDayAction;
export const addDay: CreateDayCreator = (langs, data) => (dispatch) => new Promise((resolve) => {

  createDay(langs, data).then((day) => {
    dispatch({
      type: "CREATE_DAY",
      day,
    });
  
    resolve(day);
  });
});

type DeleteDayCreator = () => DeleteDayAction;
export const delDay: DeleteDayCreator = (id) => (dispatch) => new Promise((resolve) => {

  deleteDay(id).then((day) => {
    dispatch({
      type: "DELETE_DAY",
    });
  
    resolve(day);
  });
});

type UpdateDayCreator = () => UpdateDayAction;
export const updateDay: UpdateDayCreator = (id, newData, dayIndex) => (dispatch) => new Promise((resolve) => {
  const editedDays = {};
  editedDays[dayIndex] = { ...newData, _id:id };

  updateDayData(id, newData).then((day) => {
    dispatch({
      type: "UPDATE_DAY",
      editedDays,
    });
  
    resolve(day);
  });
});

type UpdateDaysCreator = () => UpdateDaysAction;
export const updateDays: UpdateDaysCreator = (days) => (dispatch) => {
  dispatch({
    type: "UPDATE_DAYS",
    days,
  });
};

type EditDayCreator = () => EditDayAction;
export const editDay: EditDayCreator = (days) => (dispatch) => {
  dispatch({
    type: "EDIT_DAY",
    days,
  });
};

type ClearDaysCreator = () => ClearDaysAction;
export const clearDays: ClearDaysCreator = () => (dispatch) => {
  dispatch({
    type: "CLEAR_DAYS",
    days: {},
  });
};