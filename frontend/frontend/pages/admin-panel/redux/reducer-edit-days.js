// @flow
import type { Action } from '_state/actionTypes';

type State = {};
const initial = {};

type Reducer = (state: State, action: Action) => State;
const reducer: Reducer = (state = initial, action) => {
  switch (action.type) {
  case 'LOAD_DAYS': {
    return {
      ...action.days,
    };
  }
  case 'DELETE_DAY': {
    return {
      ...state,
    };
  }
  case 'UPDATE_DAY': {
    return {
      ...state,
      ...action.editedDays,
    };
  }
  default:
    return state;
  }
};

export default reducer;