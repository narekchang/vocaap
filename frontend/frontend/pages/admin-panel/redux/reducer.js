// @flow
import type { Action } from '_state/actionTypes';

type State = {};
const initial = {};

type Reducer = (state: State, action: Action) => State;
const reducer: Reducer = (state = initial, action) => {
  switch (action.type) {
  case 'LOAD_DAYS': {
    return {
      ...action.days,
    };
  }
  case 'UPDATE_DAYS': {
    return {
      ...state,
      ...action.days,
    };
  }
  case 'DELETE_DAY': {
    return {
      ...state,
    };
  }
  case 'EDIT_DAY': {
    return {
      ...state,
      ...action.days,
    };
  }
  case 'CLEAR_DAYS': {
    return {
      ...action.days,
    };
  }
  case 'CREATE_DAY': {
    const day = {};
    day[Object.keys(state).length] = action.day;

    return {
      ...state,
      ...day,
    };
  }
  default:
    return state;
  }
};

export default reducer;