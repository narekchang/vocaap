// @flow
import React from 'react';

import { branch, renderNothing, pure, mapProps, compose } from 'recompose';

import styles from './style.styl';

const CheckPage = (history) => {
  history.push('/panel');

  return (
    <div className={styles.waiting}>
      <div className={styles.preloader}>
        <div className={styles.loader} />
        <div className={styles.logo} />
      </div>
    </div>
  );
};

const enhance = compose(
  pure,
  branch(
    ({ match }) => ([ '/login', '/panel', '/feed', '/privacy-policy', '/terms-of-use' ].indexOf(match.url) > -1),
    renderNothing,
  ),
  mapProps(({ history }) => (history)),
);

export default enhance(CheckPage);
