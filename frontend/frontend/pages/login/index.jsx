// @flow
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from "lodash";

import { userLogin } from './redux/actions';

import mStyles from '../../style.styl';
import styles from './style.styl';

class Login extends React.Component<{userLogin: Object, userData: Object, history: Object}> {
  componentWillMount() {
    this.checkAuth();
  }

  login() {
    let login = {};

    if (this.props.userData.token) {
      login = { token:this.props.userData.token };
    } else {
      const email = _.get(this, "state.email");
      const password = _.get(this, "state.password");

      login = { email, password };
    }

    this.props.userLogin(login)
      .then(() => {
        // this.props.history.push('/panel');
        document.location.href = document.location.href.replace('login', 'panel');
      });
  }

  checkAuth() {
    if (this.props.userData.auth) this.props.history.push('/panel');
  }

  pressEnter = (e) => {
    if (e.keyCode === 13) this.login();
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  render() {
    return (
      <div className={mStyles.content}>
        <div className={mStyles.row}>
          <div className={`${mStyles.block} ${mStyles.col4}`}>
            <div className={mStyles.fieldWrap}>
              <input onChange={(e) => this.handleChange(e)} onKeyUp={(e) => this.pressEnter(e)} name="email" type="text" placeholder="Почта" className={`${mStyles.field} ${mStyles.mB15} ${mStyles.col12}`} />
            </div>
            <div className={mStyles.fieldWrap}>
              <input onChange={(e) => this.handleChange(e)} onKeyUp={(e) => this.pressEnter(e)} name="password" type="password" placeholder="Пароль" className={`${mStyles.field} ${mStyles.mB15} ${mStyles.col12}`} />
            </div>
            <div className={mStyles.mT15}>
              <button onClick={() => this.login()} className={`${mStyles.btn} ${mStyles.btnBlue}`}>Войти</button>
              <a
                href="https://telegram.me/edu918"
                className={mStyles.btn}
                target="_blank"
                rel="noopener noreferrer"
              >
                Забыли пароль?
              </a>
            </div>
          </div>
          <div className={`${mStyles.block} ${mStyles.col8}`}>
            <div className={styles.welcome}>
              Добро пожаловать к&nbsp;нам в&nbsp;админку. Данный раздел предназначен для переводчиков: здесь вы&nbsp;можете создать свои подборки слов для наших мобильных приложений!
            </div>
            <div className={mStyles.tACenter}>
              <a className={`${mStyles.btn} ${mStyles.btnBorder} ${mStyles.btnApp} ${mStyles.btnAppGP} ${mStyles.mS5}`} href="https://itunes.apple.com/ru/app/vocapp-%D1%81%D0%BB%D0%BE%D0%B2%D0%BE-%D0%B4%D0%BD%D1%8F/id1269275347?mt=8">
                <svg width="22" height="22" viewBox="0 0 22 22">
                  <g fill="none">
                    <g fill="#36F">
                      <path d="M5.375 13.362c0 .52-.42.944-.938.944a.941.941 0 0 1-.937-.944v-4.25c0-.522.42-.945.938-.945.517 0 .937.423.937.945v4.25zM18.5 13.362c0 .52-.42.944-.938.944a.941.941 0 0 1-.937-.944v-4.25c0-.522.42-.945.938-.945.517 0 .937.423.937.945v4.25zM10.063 18.556c0 .52-.42.944-.938.944a.941.941 0 0 1-.938-.944v-4.25c0-.522.42-.945.938-.945s.938.423.938.945v4.25zM13.813 18.556c0 .52-.42.944-.938.944a.941.941 0 0 1-.938-.944v-4.25c0-.522.42-.945.938-.945s.938.423.938.945v4.25z" />
                      <path d="M6.313 8.167v7.083c0 .521.42.944.937.944h7.5a.941.941 0 0 0 .938-.944V8.167H6.312zM11 3.444c-2.813 0-4.526 1.722-4.688 3.778h9.375C15.525 5.166 13.813 3.444 11 3.444zM9.125 6.088a.47.47 0 0 1-.469-.472c0-.26.21-.473.469-.473a.47.47 0 0 1 .469.473.47.47 0 0 1-.469.472zm3.75 0a.47.47 0 0 1-.469-.472c0-.26.21-.473.469-.473a.47.47 0 0 1 .469.473.47.47 0 0 1-.469.472z" />
                      <path d="M13.422 2.71l-.78 1.18.78.524.78-1.18zM7.793 3.227l.625.983.79-.51-.626-.982z" />
                    </g>
                  </g>
                </svg>
                Vocapp for Android
              </a>
              <a className={`${mStyles.btn} ${mStyles.btnBorder} ${mStyles.btnApp} ${mStyles.btnAppAS} ${mStyles.mS5}`} href="https://itunes.apple.com/ru/app/vocapp-%D1%81%D0%BB%D0%BE%D0%B2%D0%BE-%D0%B4%D0%BD%D1%8F/id1269275347?mt=8">
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                  <g fill="none">
                    <path fill="#36F" d="M14.416 1c-.864.034-1.91.57-2.531 1.289-.556.635-1.043 1.653-.91 2.63.963.073 1.948-.486 2.548-1.204.6-.719 1.005-1.719.893-2.715zm-.329 4.097c-1.22.016-2.352.792-2.977.792-.678 0-1.726-.759-2.835-.738-1.46.022-2.804.84-3.556 2.131-1.515 2.6-.387 6.454 1.089 8.565.722 1.032 1.582 2.194 2.713 2.151 1.09-.043 1.5-.696 2.815-.696s1.686.696 2.837.676c1.17-.023 1.914-1.055 2.631-2.09.828-1.197 1.17-2.356 1.19-2.417-.027-.011-2.284-.867-2.308-3.44-.02-2.15 1.776-3.184 1.857-3.234-1.01-1.462-2.582-1.663-3.144-1.686a2.764 2.764 0 0 0-.312-.014z" />
                  </g>
                </svg>
                Vocapp for iPhone
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({ userData:state.user }),
  dispatch => bindActionCreators({ userLogin }, dispatch),
)(Login);