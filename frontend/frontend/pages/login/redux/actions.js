// @flow
import { userAuth, getAllLangs } from '_api/user';
import type { ThunkCreator } from '_state/configureStore';

type UserLogoutAction = { type: "USER_LOGOUT" };
type UserLoginAction = {
  type: 'USER_LOGIN',
  user: Object,
};
export type UserAction =
  | UserLogoutAction
  | UserLoginAction
  ;

type UserLogoutActionCreator = () => UserLogoutAction;
export const userLogout: UserLogoutActionCreator = () => (dispatch) => new Promise((resolve) => {
  
  dispatch({
    type: "USER_LOGOUT",
  });

  resolve('logout');
});

export const userLogin: ThunkCreator<Object> = (login) => (dispatch) => new Promise((resolve) => {
  userAuth(login)
    .then((data) => {
      dispatch({
        type: "USER_LOGIN",
        user: { ...data },
      });
      resolve(data);
    })
    .catch((err) => {
      alert('Неверный логин или пароль!');
      console.log(err);
    });
});

export const setLangs: ThunkCreator<Object> = (currentLangs) => ({
  type: "USER_LANG",
  user: { currentLangs },
});

export const setLevel: ThunkCreator<Object> = (level) => ({
  type: "USER_LEVEL",
  user: { level },
});

export const getLangs: ThunkCreator<Object> = () => (dispatch) => new Promise((resolve) => {
  getAllLangs()
    .then((data) => {
      const langs = [];
      
      data.map(item => langs.push(item.val));

      dispatch({
        type: "ALL_LANGS",
        user: { langs },
      });
      resolve(langs);
    });
});