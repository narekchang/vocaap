// @flow
import type { Action } from '_state/actionTypes';

// const localAuth = JSON.parse(localStorage.getItem('reduxPersist:user') || '{}');
// if ((localAuth || {}).access_token) {
//   setToken(localAuth.access_token);
// }
type State = {};
const initial = {
  name: null,
  auth: false,
  level: 0,
};

type Reducer = (state: State, action: Action) => State;
const reducer: Reducer = (state = initial, action) => {
  switch (action.type) {
  case 'USER_LOGIN': {
    return {
      ...state,
      ...action.user,
      auth: true,
    };
  }
  case 'USER_LANG': {
    return {
      ...state,
      ...action.user,
    };
  }
  case 'ALL_LANGS': 
  case 'USER_LEVEL': {
    return {
      ...state,
      ...action.user,
    };
  }
  case 'persist/REHYDRATE': {
    return {
      ...state,
      ...action.payload.user,
    };
  }
  case 'USER_LOGOUT':
    return {
      auth: false,
    };
  case 'USER_INFO': 
    return {
      ...state,
      ...action.user,
    };
  default:
    return state;
  }
};

export default reducer;