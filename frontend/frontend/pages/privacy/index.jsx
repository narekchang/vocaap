// @flow
import React from 'react';

import mStyles from '../../style.styl';
import styles from './style.styl';

const Privacy = () => (
  <div className={mStyles.content}>
    <div className={`${mStyles.row} ${styles.row}`}>
      <h1>Privacy Policy</h1>
      <p>Last updated: May 30, 2018</p>
      <p>Edward Baksheev ("us", "we", or "our") operates the VocApp mobile application (the "Service").</p>
      <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>
      <p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
      <p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p>

      <h1>Information Collection And Use</h1>
      <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used for only to identify you and for only to manage auto-renewable subscriptions.</p>

      <h1>Log Data</h1>
      <p>When you access the Service by or through a mobile device, we may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use and other statistics ("Log Data").</p>
      <p>In addition, we may use third party services such as Facebook Bussiness that collect, monitor and analyze this type of information in order to increase our Service's functionality. These third party service providers have their own privacy policies addressing how they use such information.</p>

      <h1>Cookies</h1>
      <p>We don't use "cookies" to collect information.</p>

      <h1>Security</h1>
      <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

      <h1>Confidentiality & Security</h1>
      <p>We shall take measures to protect personal data using reasonable and industry standard security safeguards against loss or theft, as well as unauthorised access, disclosure, copying, improper use or modification.</p>

      <h1>Changes To This Privacy Policy</h1>
      <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
      <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

      <h1>Contact Us</h1>
      <p>If you have any questions about this Privacy Policy, please contact us.</p>
    </div>
  </div>
);

export default Privacy;