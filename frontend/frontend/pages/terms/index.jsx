// @flow
import React from 'react';

import mStyles from '../../style.styl';
import styles from './style.styl';

const Terms = () => (
  <div className={mStyles.content}>
    <div className={`${mStyles.row} ${styles.row}`}>
      <h1>Terms of Use</h1>
      <p>Last updated: May 30, 2018</p>
      <p>These Terms of Use (“Terms”) apply to your access and use of our applications (“Service”) provided by VocApp. (“VocApp” or “we”). By downloading VocApp mobile application, you agree to these Terms. If you do not agree to these Terms, do not access or use our Services.</p>
      <p>If you have any questions about these Terms or our Services, please contact us at behance.net/edu918</p>

      <h1>Description of Services</h1>
      <p>VocApp is an application based software that uses own data base of the words and phrases for learning foreign languages. All content is original.</p>

      <h1>Eligibility</h1>
      <p>You must be at least 6 years of age to access or use our Services. Service content does not have swear words and phrases. If you are accessing or using our Services on behalf of another person or entity, you represent that you are authorized to accept these Terms on that person or entity’s behalf and that the person or entity agrees to be responsible to us if you or the other person or entity violates these Terms.</p>

      <h1>Intellectual Property</h1>
      <p>The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of Eduard Baksheev and its licensors. The Service is protected by copyright. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Eduard Baksheev. Our Service may contain links to third-party web sites or services.</p>

      <h1>Changes</h1>
      <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
      <p>By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>

      <h1>Contact Us</h1>
      <p>If you have any questions about these Terms, please contact us.</p>

      <h1>Terms of Use</h1>
      <p>Last updated: May 30, 2018</p>
      <p>These Terms of Use (“Terms”) apply to your access and use of our applications (“Service”) provided by VocApp. (“VocApp” or “we”). By downloading VocApp mobile application, you agree to these Terms. If you do not agree to these Terms, do not access or use our Services.</p>
      <p>If you have any questions about these Terms or our Services, please contact us at behance.net/edu918</p>

      <h1>Description of Services</h1>
      <p>VocApp is an application based software that uses own data base of the words and phrases for learning foreign languages. All content is original.</p>

      <h1>Eligibility</h1>
      <p>You must be at least 6 years of age to access or use our Services. Service content does not have swear words and phrases. If you are accessing or using our Services on behalf of another person or entity, you represent that you are authorized to accept these Terms on that person or entity’s behalf and that the person or entity agrees to be responsible to us if you or the other person or entity violates these Terms.</p>

      <h1>Intellectual Property</h1>
      <p>The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of Eduard Baksheev and its licensors. The Service is protected by copyright. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Eduard Baksheev. Our Service may contain links to third-party web sites or services.</p>

      <h1>Changes</h1>
      <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
      <p>By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>

      <h1>Contact Us</h1>
      <p>If you have any questions about these Terms, please contact us.</p>
    </div>
  </div>
);

export default Terms;