import { normalize } from 'normalizr';
import dotProp from "dot-prop-immutable";
import _ from 'lodash';
import { taskSchema } from './../../state/schemas';

const initial = {};

const mergeDedupe = arr => [...new Set([].concat(...arr))];

export default function (state = initial, action) {
  switch (action.type) {
  case "QUIZ_LOADED": {
    const normalized = normalize(_.get(action, 'payload.tasks', []), [taskSchema]);
    return _.merge({}, state, normalized.entities.tasks);
  }
  case "NEXT_QUESTION_LOADED": {
    const q = action.payload;
    const oldQuestions = _.get(state, `${q.taskId}.questions`, []);
    const questions = mergeDedupe(oldQuestions.concat([q.id]));
    return _.merge({}, state, {
      [`${q.taskId}`]: {
        questions,
      },
    });
  }
  case "TASK_RESULT_LOADED": {
    const { taskId, stars } = action.payload;
    return _.merge(state, {
      [`${taskId}`]: {
        ...state[taskId],
        completed: true,
        stars: Math.max(_.get(state, `${taskId}.stars`), stars),
      },
    });
  }
  case "MAKE_TASK_ACTIVE": {
    const { taskId } = action.payload;
    return dotProp.set(
      state,
      `${taskId}.active`,
      true,
    );
  }
  default:
    return state;
  }
}
