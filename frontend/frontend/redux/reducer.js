// @flow
import userReducer, { type State as UserState } from '_pages/login/redux/reducer';
import daysReducer, { type State as DaysState } from '_pages/admin-panel/redux/reducer';
import editDaysReducer, { type State as editDaysState } from '_pages/admin-panel/redux/reducer-edit-days';
import PostsReducer, { type State as PostsState } from '_pages/admin-feed/redux/reducer';
import editPostReducer, { type State as editPostsState } from '_pages/admin-feed/redux/reducer-edit-days';

const reducers = {
  user: userReducer,
  days: daysReducer,
  editedDays: editDaysReducer,
  posts: PostsReducer,
  editedPosts: editPostReducer,
};

export type State = {
  user: UserState,
  days: DaysState,
  editedDays: editDaysState,
  posts: PostsState,
  editedPosts: editPostsState,
};

export default reducers;
