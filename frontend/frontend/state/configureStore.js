// @flow
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import type {
  Store as ReduxStore,
  Dispatch as ReduxDispatch,
} from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createHashHistory from 'history/createHashHistory';
import createBrowserHistory from 'history/createBrowserHistory';
import { logger } from 'redux-logger';
import multi from 'redux-multi';
import thunk from 'redux-thunk';
// $FlowFixMe
import { persistStore } from 'redux-persist';
import reducers, { type State } from '_redux/reducer';

import DevTools from './../containers/DevTools';
import type { Action } from './actionTypes';

// const reducersMap = reducers;
const reducersMap = {
  ...reducers,
  router: routerReducer,
};
// type Reducers = typeof reducersMap;

/* eslint-disable no-undef */
// type $ExtractFunctionReturn = <V>(v: (...args: any) => V) => V;
// export type State = $ObjMap<Reducers, $ExtractFunctionReturn>;
/* eslint-enable no-undef */

export type { State };
export type Store = ReduxStore<State, Action>;
export type GetState = () => State;
export type Thunk<A, R> = ((Dispatch, GetState) => Promise<void> | void | Promise<R>) => A;
export type ThunkCreator<ARG, RES> = (...args: Array<ARG>) => (Dispatch, GetState) => RES | Promise<RES>;
export type Dispatch =
  & ReduxDispatch<Action>
  & Thunk<Action, *>
;

const dev = process.env.NODE_ENV === 'development';

export const history = !dev ? createHashHistory() : createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const enhancer = compose(...[
  // Middleware you want to use in development:
  applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware,
    multi,
    thunk,
    logger,
  ),
  // Required! Enable Redux DevTools with the monitors you chose
  dev ? DevTools.instrument() : null,
].filter(Boolean));


type ConfigureStore = (initialState: State) => Promise<Store>;
export const configureStore: ConfigureStore = (initialState) => new Promise((resolve, reject) => {
  try {

    const store: Store = createStore(
      combineReducers(reducersMap),
      initialState,
      enhancer,
    );
    // sagaMiddleware.run(rootSaga);
    persistStore(
      store,
      {
        whitelist: ['user'],
      },
      () => resolve(store),
    );
  } catch (e) {
    reject(e);
  }
});

export default configureStore;

