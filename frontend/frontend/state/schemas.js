import { schema } from 'normalizr';
import _ from 'lodash';

const mergeDedupe = arr => [...new Set([].concat(...arr))];

export const questionSchema = new schema.Entity('questions');
// export const prizeSchema = new schema.Entity('prizes');

export const taskSchema = new schema.Entity('tasks', {
  questions: [questionSchema],
}, {
  processStrategy: (entity) => Object.assign({ questions:[]}, entity),
});

export const quizSchema = new schema.Entity('quizes', {
  tasks: [taskSchema],
  // prizes: [prizeSchema],
}, {
  processStrategy: (entity) => {
    const newEntity = Object.assign({ tasks:[], prizes:[], winners:[]}, entity);
    [ 'tasks', 'prizes' ].forEach(k => {
      newEntity[k] = _.sortBy(newEntity[k], ['id']);
    });
    return newEntity;
  },
  mergeStrategy: (entityA, entityB) => ({
    ...entityA,
    ...entityB,
    tasks: mergeDedupe(entityA.tasks.concat(entityB.tasks)),
    prizes: mergeDedupe(entityA.prizes.concat(entityB.prizes)),
  }),

});

export const brandSchema = new schema.Entity('brands', {
  quizes: [quizSchema],
}, {
  processStrategy: (entity) => Object.assign({ quizes:[]}, entity),
});
