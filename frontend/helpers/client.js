// @flow
export type UnixTime = number;
export type User = {
  first_name: string,
  last_name: string,
  email: string,
  phone: string,
  avatar: string
};
export type Brand = { title: string, logo_url: string };
export type Quest = {
  id: number,
  title: string,
  brand: Brand,
  story: string,
  timings: Object,
  background_url: string
};
export type Stage = {
  id: number,
  title: string,
  text: string,
  images: Array<string>,
  actions: Array<Action>,
  final: "3" | "5"
};
export type Action = { id: number, title: string };
