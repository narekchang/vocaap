/* eslint-disable */
module.exports = {
  rules: {
    "pugjsx-use-var": require("./rules/pugjsx-use-var"),
  },
  configs: {
    recommended: {
      plugins: [
        'react'
      ],
      globals: {
        pug: true,
      },
      parserOptions: {
        ecmaFeatures: {
          jsx: true
        }
      },
      rules: {
        'pug-react-usevar/pugjsx-use-var': 2,
      }
    },
  }
};