/* eslint-disable */
var render = require('pug').render;
var transform = require('babel-core').transform;


module.exports = {
  meta: {
    docs: {}
  },

  create (context) {
    return {
      TaggedTemplateExpression (node) {
        if (node.tag && node.tag.name === 'pug') {
          context.markVariableAsUsed('styles');
          const raw = node.quasi.quasis[0].value.raw;

          const splitedRaw = raw.split('\n').filter((str) => str !== '' && str.match(/^\s*$/g) === null);
          const rootIndent = /^\s*/.exec(splitedRaw[0])[0];
          const fixedRaw = splitedRaw.map((raw) => {
            const spaceRegExp = new RegExp(`^${ rootIndent}`);
            return raw.replace(spaceRegExp, '');
          }).join('\n');
          const html = render(fixedRaw, {
            basedir: context.parserPath.replace(/node_modules(.*)/, '')
          }).replace(/"\{/g, '{').replace(/class="([^"]+)/g, 'className={styles.$1 || \'$1\'}').replace(/for="/g, 'htmlFor="').replace(/\}"/g, '}').replace(/\};"/g, '}').replace(/\\\`/g, '`');
          const _transform = transform(html, {
            presets: ['react']
          });
          const ast = _transform.ast;


          eeeeee(html, context);

        }
      }
    };
  }
};

function eeeeee(code, context) {
  const espree = require("espree");
  const utils = require("esprima-ast-utils");
  const ast = espree.parse(code, {
    range: true,
    // attach line/column location information to each node
    loc: true,

    // create a top-level tokens array containing all tokens
    tokens: true,

    // set to 3, 5 (default), 6, 7, or 8 to specify the version of ECMAScript syntax you want to use.
    // You can also set to 2015 (same as 6), 2016 (same as 7), or 2017 (same as 8) to use the year-based naming.
    ecmaVersion: 6,
    // specify which type of script you're parsing (script or module, default is script)
    sourceType: "script",
    ecmaFeatures: {
      jsx: true,
      // enable return in global scope
      globalReturn: true,
      impliedStrict: true,
      experimentalObjectRestSpread: true
    }
  });


  utils.traverse(ast, node => {
    if (node.type === "JSXOpeningElement") {
      var name;
      if (node.name.namespace && node.name.namespace.name) {
        // <Foo:Bar>
        name = node.name.namespace.name;
      } else if (node.name.name) {
        // <Foo>
        name = node.name.name;
      } else if (node.name.object) {
        // <Foo...Bar>
        let parent = node.name.object;
        while (parent.object) {
          parent = parent.object;
        }
        name = parent.name;
      } else {
        return;
      }

      context.markVariableAsUsed(name);

    }
    if (node.type === "CallExpression") {
      if (node.callee && node.callee.type === 'Identifier') {
        context.markVariableAsUsed(node.callee.name);
      }
    }
    if (node.type === "JSXExpressionContainer") {
      if (node.expression && node.expression.type === 'Identifier') {
        context.markVariableAsUsed(node.expression.name);
      }
    }
  });

}
