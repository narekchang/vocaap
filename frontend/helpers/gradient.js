import tinycolor from 'tinycolor2';
import _ from 'lodash';


export default class Gradient {
  constructor(props) {
    const colors = _.sortBy(props, ['pos']);

    const first = _.first(props);
    if (first && first.pos !== 0) {
      colors.unshift({ ...first, pos:0 });
    }

    const last = _.last(props);
    if (last && last.pos !== 1) {
      colors.push({ ...last, pos:1 });
    }

    this.colors = colors;
  }
  colorAtPos(pos) {
    if (pos >= 1) return _.last(this.colors.color);
    if (pos <= 0) return _.first(this.colors.color);

    const index = _.findLastIndex(this.colors, c => c.pos <= pos);

    if (index > -1) {
      const a = this.colors[index];
      const b = this.colors[index + 1];
      const mix = (pos - a.pos) / (b.pos - a.pos) * 100;
      const color = tinycolor.mix(a.color, b.color, mix).toHexString();

      return color;
    }
  }
}