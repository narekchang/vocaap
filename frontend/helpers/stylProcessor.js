const babel = require('babel-core');
const md5 = require('js-md5');

module.exports = {
  process: (src, filename) => {
    const rp = filename.replace(process.cwd(), '');
    const hsh = new Buffer(md5(rp)).toString('base64').substr(1, 5);
    return babel.transform(`
idObj = new Proxy({}, {
  get: function getter(target, key) {
    if (key === '__esModule') {
      return false;
    }
    return key + '${hsh}';
  }
});

module.exports = idObj;
    `).code;

  },
};
