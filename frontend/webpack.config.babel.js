import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackErrorNotificationPlugin from 'webpack-error-notification';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';
import BabiliPlugin from 'babili-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import DotenvPlugin from 'webpack-dotenv-plugin';
import path from 'path';
import yargs from 'yargs'; // eslint-disable-line
import { loaders, loaderOptions } from './loaders.config';
import ShowCompilationErrors from './helpers/webpackBuildDebugger';

const NODE_ENV = process.env.NODE_ENV === 'production' ? 'production' :
  'development';
const dev = NODE_ENV === 'development';
const prod = NODE_ENV === 'production';
const HOST = process.env.HOST || "0.0.0.0";
const PORT = yargs.argv.port || "3000";

export default {
  entry: {
    frontend: [
      dev ? 'react-hot-loader/patch' : null,
      './frontend/index.jsx',
    ].filter(Boolean),
  },
  output: {
    path: path.join(__dirname, 'www'),
    publicPath: '/',
    filename: 'build/[name].js?[hash]',
  },
  resolve: {
    modules: [
      './node_modules/',
    ],
    extensions: [ '*', '.jsx', '.js', '.css' ],
    alias: {
      '_api': path.resolve(__dirname, 'frontend', 'api'),
      '_constants': path.resolve(__dirname, 'frontend', 'constants'),
      '_containers': path.resolve(__dirname, 'frontend', 'containers'),
      '_components': path.resolve(__dirname, 'frontend', 'components'),
      '_pages': path.resolve(__dirname, 'frontend', 'pages'),
      '_redux': path.resolve(__dirname, 'frontend', 'redux'),
      '_state': path.resolve(__dirname, 'frontend', 'state'),
    },
  },

  resolveLoader: {
    modules: ['./node_modules/'],
    moduleExtensions: [ '-loader', '*' ],
    extensions: [ '*', '.js' ],
  },

  devServer: {
    watchOptions: {
      aggregateTimeout: 100,
      poll: 1000,
    },
    hot: true, inline: true,
    historyApiFallback: {
      rewrites: [
        { from:/^\/admin/, to:'/admin/index.html' },
      ],
    },
    host: HOST, port: PORT,
  },

  module: {
    loaders,

    // noParse: [
    //   /\.min\.js/,
    //   /jquery.js/,
    // ],
  },

  devtool: NODE_ENV === 'production' ? false : 'inline-source-map',

  plugins: [
    // new LodashModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: 'Vocapp',
      template: 'frontend/index.pug',
      filename: 'index.html',
      chunks: [ 'react-build', 'vendor', 'frontend' ],
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].css?[hash]',
      allChunks: true,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'build/vendor.js',
      minChunks(module, count) {
        const { context } = module;
        return context && context.indexOf('node_modules') >= 0 && count >= 2;
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'react-build',
      filename: 'build/react-build.js',
      minChunks(module) {
        const { context } = module;
        return context && context.indexOf('react') >= 0;
      },
    }),
    new DotenvPlugin({
      sample: './_env',
      path: dev ? './.env' : './.env.prod',
      allowEmptyValues: true,
    }),

    loaderOptions,
    
    prod && new BundleAnalyzerPlugin({
      analyzerMode: 'static',
    }),
    prod && new ProgressBarPlugin(),
    prod && new BabiliPlugin({
      removeConsole: true,
    }, {
      comments: false,
    }),
    prod && ShowCompilationErrors,
    
    dev && new WebpackErrorNotificationPlugin(),
    dev && new webpack.NoEmitOnErrorsPlugin(),
  ].filter(Boolean),
};
